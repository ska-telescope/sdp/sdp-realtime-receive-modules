# -*- coding: utf-8 -*-

"""Module init code."""

from importlib.metadata import version

__version__ = version("ska-sdp-realtime-receive-modules")

__all__ = ("receiver", "receivers", "consumers")
