from realtime.receive.modules.consumers.consumer import Consumer


class NullConsumer(Consumer):
    """A consumer that discards all payloads"""

    async def consume(self, visibility):
        pass

    async def astop(self):
        pass
