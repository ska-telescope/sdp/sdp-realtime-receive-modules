from .consumer import Consumer, Visibility


class AccumulatingConsumer(Consumer):
    """A consumer that keeps all visibilities around for later inspection."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.visibilities: list[Visibility] = []
        self.scans_started = []
        self.scans_ended = []

    async def consume(self, visibility: Visibility):
        self.visibilities.append(visibility)

    async def start_scan(self, scan_id: int) -> None:
        self.scans_started.append(scan_id)

    async def end_scan(self, scan_id: int) -> None:
        self.scans_ended.append(scan_id)
