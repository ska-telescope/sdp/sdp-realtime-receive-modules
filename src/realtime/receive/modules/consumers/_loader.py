import importlib
import logging

from realtime.receive.core.common import from_dict
from realtime.receive.core.uvw_engine import UVWEngine

from realtime.receive.modules.consumers.accumulating_consumer import AccumulatingConsumer
from realtime.receive.modules.consumers.consumer import Consumer
from realtime.receive.modules.consumers.example_consumer import ExampleConsumer
from realtime.receive.modules.consumers.mswriter import MSWriterConsumer
from realtime.receive.modules.consumers.null_consumer import NullConsumer
from realtime.receive.modules.consumers.plasma_writer import PlasmaWriterConsumer
from realtime.receive.modules.tm.base_tm import TelescopeManager

from ._config import Config

logger = logging.getLogger(__name__)


def _build_if_not_concrete_config_instance(config: Config, config_class: type):
    assert issubclass(config_class, Config)
    return config if isinstance(config, config_class) else config_class()


_inbuilt_consumers: dict[str, Consumer] = {
    "accumulating_consumer": AccumulatingConsumer,
    "mswriter": MSWriterConsumer,
    "null_consumer": NullConsumer,
    "plasma_writer": PlasmaWriterConsumer,
    "example_consumer": ExampleConsumer,
}


def _get_consumer_class(config: Config):
    if consumer_class := _inbuilt_consumers.get(config.name):
        klass = consumer_class
    else:
        modname, classname = config.name.rsplit(".", 1)
        m = importlib.import_module(modname)
        klass = getattr(m, classname)

    if not issubclass(klass, Consumer):
        logger.warning(
            "Resolved %s consumer, but it is not a subclass of %s",
            config.name,
            klass.__name__,
        )

    return klass


def create_config(**kwargs):
    """Generate a Config object from the given kwargs"""
    generic_config = from_dict(Config, kwargs)
    return from_dict(_get_consumer_class(generic_config).config_class, kwargs)


def create(
    config: Config,
    tm: TelescopeManager,
    uvw_engine: UVWEngine = None,
):
    """Create a consumer with the given config"""

    consumer_class = _get_consumer_class(config)
    config = _build_if_not_concrete_config_instance(config, consumer_class.config_class)
    logger.info("Creating consumer with config: %r", config)
    return consumer_class(config, tm, uvw_engine)
