# -*- coding: utf-8 -*-


""" Takes a SPEAD2 HEAP and tests it for completeness. This task has been generated as part of the
    the YAN-716 ticket. It will initially be used to test whether this system can write at
    AA0.5 rates
"""

import logging

from overrides import overrides

from realtime.receive.modules.consumers.consumer import Consumer

logger = logging.getLogger(__name__)


class ExampleConsumer(Consumer):
    """
    A heap consumer that captures the packets and quickly inspects them.
    """

    @overrides
    def __init__(self):
        super().__init__(None, None, None)

    @overrides
    async def consume(self, visibility):
        """
        Entry point invoked by the receiver each time a Visibility is consumed.

        :param visibility: the visibility dataset and its associated metadata
        """
        await self.operate_on_visibility(visibility)

    @overrides
    async def astop(self):
        pass

    async def operate_on_visibility(self, visibility):
        """Operates on the visibility."""

        # Perform the operation on the data
        assert visibility.time.any()
        assert visibility.vis.any()
