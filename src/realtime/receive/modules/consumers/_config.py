import dataclasses


@dataclasses.dataclass
class Config:
    """Common configuration for all consumers"""

    name: str = "mswriter"
    """
    The fully-qualified name of the consumer class to use. Built-in consumers
    can be specified by simple module name (e.g., `mswriter`).
    """
