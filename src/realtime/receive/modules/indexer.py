class Indexer:
    """
    A class that creates indexes for different given amounts

    This class simply creates a rolling index for the given arbitrary values.
    If a value has an index, that is returned; otherwise a new index is created
    and stored for the new value.

    This simple mechanism is used, for example, for guessing the time index for
    a received ICD payload (which is later on used, for example, to calculate
    the row number under which the visibilities in the payload should be written
    to in a Measurement Set). These payloads contain a timestamp but no
    reference to their T0. Using this class to infer a time index for these
    payloads works by assuming payloads are received in order (which might not
    hold), and because payloads contain data for all visibilities (but maybe a
    portion of the channels), so the MS row calculation doesn't break.
    """

    def __init__(self):
        self._indices = {}

    def get_index(self, value):
        """Get the index of the given value"""
        index = self._indices.get(value, None)
        if index is None:
            index = len(self._indices)
            self._indices[value] = index
        return index

    def reset(self):
        """Remove all indices"""
        self._indices = {}
