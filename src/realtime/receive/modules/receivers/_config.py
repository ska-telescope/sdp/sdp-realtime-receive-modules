import dataclasses


@dataclasses.dataclass
class Config:
    """Base configuration for all receivers"""

    method: str = "spead2_receivers"
    """The method used to receive"""
