from .base_tm import TelescopeManager
from .hardcoded_tm import HardcodedTelescopeManager
from .ms_tm import MeasurementSetTM
from .sched_tm import SKATelescopeManager
