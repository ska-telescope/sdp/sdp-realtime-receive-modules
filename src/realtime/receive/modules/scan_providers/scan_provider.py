"""Base class definition for all scan providers"""

import abc
import logging
from typing import Awaitable, Callable

from realtime.receive.core.scan import Scan

from realtime.receive.modules.data_reception_handler import DataReceptionHandler
from realtime.receive.modules.scan_lifecycle_handler import ScanLifecycleHandler

logger = logging.getLogger(__name__)


class ScanProvider(DataReceptionHandler):
    """A class that can return Scan objects for given scan IDs"""

    def __init__(self):
        self._scan_lifecycle_handlers = []
        self._current_scan: Scan | None = None

    def add_scan_lifecycle_handler(self, hanler: ScanLifecycleHandler) -> None:
        """Adds a handler that will be invoked upon scan transitions"""
        self._scan_lifecycle_handlers.append(hanler)

    async def _notify_scan_transition(
        self, scan_id: int, callbacks: list[Callable[[int], Awaitable[None]]], transition: str
    ) -> None:
        for callback in callbacks:
            try:
                await callback(scan_id)
            except Exception:  # pylint: disable=broad-except
                logger.exception("Unexpected error while invoking %s callback", transition)

    async def _notify_start_scan(self, scan_id: int) -> None:
        await self._notify_scan_transition(
            scan_id, [handler.start_scan for handler in self._scan_lifecycle_handlers], "start"
        )

    async def _notify_end_scan(self, scan_id: int) -> None:
        await self._notify_scan_transition(
            scan_id, [handler.end_scan for handler in self._scan_lifecycle_handlers], "end"
        )

    @property
    def current_scan(self) -> Scan | None:
        """The current scan being observed"""
        return self._current_scan

    @current_scan.setter
    def current_scan(self, scan: Scan | None) -> None:
        if scan:
            logger.info("Setting current scan to %d", scan.scan_number)
        else:
            logger.info("Clearing current scan")
        self._current_scan = scan

    async def _set_and_start_current_scan(self, scan: Scan) -> None:
        self.current_scan = scan
        await self._notify_start_scan(scan.scan_number)

    async def _unset_and_end_current_scan(self) -> None:
        assert self.current_scan
        current_scan = self.current_scan
        await self._notify_end_scan(current_scan.scan_number)
        self.current_scan = None

    @abc.abstractmethod
    def get(self, scan_id: int) -> Scan | None:
        """
        Return the Scan object associated to the given scan ID

        If no scan is known for the given ID, None is returned instead

        :param scan_id: The Scan ID
        """

    async def aclose(self):
        """Release all underlying resources"""

    # async context manager protocol
    async def __aenter__(self):
        return self

    async def __aexit__(self, typ, value, traceback):
        await self.aclose()
