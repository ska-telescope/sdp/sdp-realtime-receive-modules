"""Scan provider directly receiving an AssignResources command"""

import itertools

from overrides import override
from realtime.receive.core.scan import Scan
from realtime.receive.core.scan_utils import parse_scantypes_from_assignres

from .cbf_triggered_scan_provider import CBFTriggeredScanProvider


class AssignResourcesScanProvider(CBFTriggeredScanProvider):
    """
    A ScanProvider that creates Scans based on the contents of a AssignResources
    command.

    An AssignResources command alone doesn't contain all the necessary data to
    map Scan IDs to ScanTypes (and the rest of their related metadata). This
    bypasses this by mapping consecutive Scans to consecutive ScanTypes in a
    round-robin fashion.
    """

    def __init__(self, assign_resources_command: str):
        """
        Create a new AssignResourcesScanProvider for the given AssignResources
        command.

        :param assign_resources_command: The AssignResources command
        """
        super().__init__()
        scan_types = parse_scantypes_from_assignres(assign_resources_command)
        self._scan_types_iterator = itertools.cycle(scan_types)

    @override
    async def first_scan_data_received(self, scan_id: int) -> None:
        await self._set_and_start_current_scan(Scan(scan_id, next(self._scan_types_iterator)))

    @override
    def get(self, scan_id: int) -> Scan:
        return Scan(scan_id, next(self._scan_types_iterator))
