"""Base class for most/all non-production scan providers"""

import abc
import logging

from overrides import override

from .scan_provider import ScanProvider

logger = logging.getLogger(__name__)


class CBFTriggeredScanProvider(ScanProvider, abc.ABC):
    """
    A Scan provider that invokes its start/stop_scan callbacks as soon as the
    receiver has received the start-of-stream heaps from CBF, and has closed
    all its streams, respectively.
    """

    @override
    async def last_scan_data_received(self) -> None:
        if self._current_scan is None:
            logger.warning(
                "CBF scan ended, but we didn't detect it starting earlier, "
                "skipping call to end_scan in handlers"
            )
            return
        await self._notify_end_scan(self._current_scan.scan_number)
