import functools

from overrides import override
from realtime.receive.core.msutils import MeasurementSet
from realtime.receive.core.scan import Scan

from .cbf_triggered_scan_provider import CBFTriggeredScanProvider

MAX_CONCURRENT_SCANS = 2
"""
Maximum number of scans ever expected to be received concurrently. 2 is already
a stretch, but is useful for tests.
"""


class MSScanProvider(CBFTriggeredScanProvider):
    """
    A ScanProvider that reads Scan information from an input Measurement Set.
    """

    def __init__(self, measurement_set: str | MeasurementSet):
        super().__init__()
        _ms = measurement_set
        if isinstance(measurement_set, str):
            _ms = MeasurementSet.open(measurement_set)
        self._scan_type = _ms.calculate_scan_type()
        if isinstance(measurement_set, str):
            _ms.close()

    @override
    async def first_scan_data_received(self, scan_id: int) -> None:
        await self._set_and_start_current_scan(Scan(scan_id, self._scan_type))

    @override
    @functools.lru_cache(MAX_CONCURRENT_SCANS)
    def get(self, scan_id: int) -> Scan | None:
        return Scan(scan_id, self._scan_type)
