"""Main production workhorse scan provider, driven by  SDP Config DB contents"""

import abc
import asyncio
import concurrent.futures
import logging
import threading

from overrides import override
from realtime.receive.core.scan import Scan, ScanType
from realtime.receive.core.scan_utils import parse_scantypes_from_execblock
from ska_sdp_config import Config, ExecutionBlock

from realtime.receive.modules.utils.delayed_task import DelayedTask

from .scan_provider import ScanProvider

logger = logging.getLogger(__name__)


class SdpConfigDBUpdateHandler(abc.ABC):
    """
    Interface for objects that want to be notified about changes to the SDP config DB as monitored
    by the SdpConfigScanProvider. This restricts update notifications to changes to the Execution
    Block under monitoring.
    """

    @abc.abstractmethod
    async def db_updated(self, eb_state: dict) -> None:
        """Called every time the ExecutionBlock is updated"""


class SdpConfigScanProvider(ScanProvider, SdpConfigDBUpdateHandler):
    """
    A ScanProvider that creates Scans based on the contents of the SDP
    configuration database, usually backed by an etcd server.

    For details on the Subarray obsStates and the commands that trigger their transitions
    see https://developer.skao.int/projects/ska-control-model/en/0.3.1/obs_state.html.
    """

    _STOP_SENTINEL = object()

    def __init__(
        self,
        execution_block_id,
        sdp_config: Config,
        end_scan_max_delay: float | None = None,
    ):
        """
        Create a new ScanProvider for the given configuration DB response.

        :param configuration_response: The response from the configuration database
        :param sdp_config: An SDP Configuration DB client
        :param end_scan_max_delay: Maximum waiting time, in seconds, before propagating
         an EndScan command.
        """
        super().__init__()
        self._scans: dict[int, Scan] = {}
        self._execution_block_id: str = execution_block_id
        self._db_update_handlers: list[SdpConfigDBUpdateHandler] = [self]
        self._sdp_config = sdp_config
        self._watcher = None
        self._watcher_lock = threading.Lock()
        self._closed = False
        self._current_cbf_scan_id: int | None = None
        self._first_db_update_received: bool = False
        for txn in self._sdp_config.txn():
            execution_block: ExecutionBlock = txn.execution_block.get(execution_block_id)
            if not execution_block:
                raise ValueError(f"No EB in SDP config with id='{execution_block_id}'")
        self._scan_types: dict[str, ScanType] = {
            scan_type.scan_type_id: scan_type
            for scan_type in parse_scantypes_from_execblock(execution_block.model_dump())
        }
        logger.info(
            "Loaded %d scan types from ExecutionBlock '%s'",
            len(self._scan_types),
            execution_block_id,
        )
        self._monitoring_task: asyncio.Task | None = None
        self._do_end_scan_task: DelayedTask | None = None
        self._end_scan_max_delay = end_scan_max_delay

    @property
    def end_scan_pending(self) -> bool:
        """Whether an EndScan command has been seen but not processed yet"""
        return self._do_end_scan_task is not None

    def add_db_update_handler(self, handler: SdpConfigDBUpdateHandler) -> None:
        """Adds a handler that will be invoked upon SDP Config DB updates"""
        self._db_update_handlers.append(handler)

    async def _notify_db_update(self, eb_state: dict) -> None:
        for handler in self._db_update_handlers:
            try:
                await handler.db_updated(eb_state)
            except Exception:  # pylint: disable=broad-except
                logger.exception("Unexpected error while invoking db_updated callback")

    async def __aenter__(self):
        await super().__aenter__()
        self._monitoring_task = asyncio.create_task(self._do_scan_monitoring())
        return self

    async def _do_scan_monitoring(self):
        """
        Runs background monitoring for new Scans. It does so by awaiting on the
        long-running _monitor_new_scans method, which runs on a separate thread.
        """
        executor = concurrent.futures.ThreadPoolExecutor(max_workers=1)
        loop = asyncio.get_running_loop()
        queue: asyncio.Queue[tuple[dict, dict]] = asyncio.Queue()
        monitoring_thread_task = loop.run_in_executor(
            executor, self._monitor_db_updates, loop, queue
        )
        try:
            while True:
                eb_state = await queue.get()
                if eb_state is SdpConfigScanProvider._STOP_SENTINEL:
                    break
                await self._notify_db_update(eb_state)
        finally:
            await monitoring_thread_task
            executor.shutdown()

    def _monitor_db_updates(self, loop: asyncio.AbstractEventLoop, queue: asyncio.Queue):
        """
        Continuously monitors the configuration database for changes on the
        Execution Block and corresopnding Subarray.
        """
        logger.info(
            "Monitoring EB '%s' state in the background",
            self._execution_block_id,
        )

        def end_monitoring():
            asyncio.run_coroutine_threadsafe(queue.put(SdpConfigScanProvider._STOP_SENTINEL), loop)

        # shield against updates that don't change the EB state contents
        # this doesn't happen anymore since ska-sdp-config 0.10.2, but realtime-receive components
        # do not manage the EB state
        last_eb_state = None
        for watcher in self._sdp_config.watcher():
            if self._closed:
                end_monitoring()
                return
            with self._watcher_lock:
                self._watcher = watcher
            for txn in watcher.txn():
                eb_state = txn.execution_block.state(self._execution_block_id).get()
                if eb_state is None:
                    logger.error(
                        "EB '%s' has no state, will keep trying, but expect trouble",
                        self._execution_block_id,
                    )
                    break
                if eb_state != last_eb_state:
                    asyncio.run_coroutine_threadsafe(queue.put(eb_state), loop)
                last_eb_state = eb_state

    @override
    async def db_updated(self, eb_state: dict):
        """Handler to the contents of the given Execution Block being updated, if any."""
        logger.info("Received ExecutionBlock update: %s", eb_state)
        scan_id = eb_state.get("scan_id")
        scan_type_id = eb_state.get("scan_type")

        # At receiver startup the EB shouldn't be scanning, and thus we don't want to treat this
        # as an EndScan command. If the EB *is* scanning we shouldn't announce a Scan command
        # either, but we should warn about this unexpected EB state
        if not self._first_db_update_received:
            self._first_db_update_received = True
            if scan_id is not None:
                logger.warning(
                    (
                        "Execution Block has an associated scan_id=%d, maybe the receiver "
                        "crashed and was restarted in the middle of an obseration"
                    ),
                    scan_id,
                )
                # make our internal state consistent though, so that EndScan sees a current scan
                self.current_scan = Scan(scan_id, self._scan_types[scan_type_id])
            return

        if scan_id is not None:
            if not scan_type_id:
                logger.warning("scan_type_id is empty, can't process Scan command")
                return
            await self._subarray_scan(scan_id, scan_type_id)
        else:
            await self._subarray_end_scan()

    async def _subarray_end_scan(self):
        # e.g., if the receiver gets restarted after an EndScan but before Scan
        if not self.current_scan:
            logger.warning(
                "EndScan detected, but receiver didn't react to previous Scan, ignoring"
            )
            return
        logger.info(
            "Detected EndScan command for scan_id=%d",
            self.current_scan.scan_number,
        )
        # CBF streams already stopped (or we explicitly asked for no delay)
        if self._current_cbf_scan_id is None or self._end_scan_max_delay is None:
            await self._unset_and_end_current_scan()
            return
        self._do_end_scan_task = DelayedTask(
            self._end_scan_max_delay, self._unset_and_end_current_scan
        )

    async def _subarray_scan(self, scan_id: id, scan_type_id: str):
        if self._current_scan:
            # two successive Scan commands might be observed if EndScan and Scan are issued
            # in such a quick succession that the watcher, as it iterates after being triggered
            # by the updates, picks up the new Scan's scan_id from the EB state, skipping the
            # null value that would be there due to the End command.
            if self._current_scan.scan_number == scan_id:
                logger.warning("Scan command has same scan_id as current scan, ignoring")
                return
            logger.warning(
                "Received Scan command, but there is a current scan, forcibly terminating it"
            )
            if self.end_scan_pending:
                await self._force_pending_end_scan()
            else:
                await self._unset_and_end_current_scan()
        # scan_id has been seen already, shouldn't happen, but let's try our best
        if scan_id in self._scans:
            logger.warning("Detected Scan command with used scan_id=%d, expect trouble!", scan_id)
        self._scans[scan_id] = Scan(scan_id, self._scan_types[scan_type_id])
        logger.info(
            "Detected new Scan with scan_id=%d, scan_type_id=%s",
            scan_id,
            scan_type_id,
        )
        await self._set_and_start_current_scan(self._scans[scan_id])

    @override
    def get(self, scan_id: int) -> Scan | None:
        return self._scans.get(scan_id, None)

    async def aclose(self):
        self._closed = True
        with self._watcher_lock:
            if self._watcher:
                self._watcher.trigger()
                self._watcher = None
        if self._monitoring_task:
            await self._monitoring_task
            self._monitoring_task = None
        await self._force_pending_end_scan()

    @override
    async def first_scan_data_received(self, scan_id: int) -> None:
        self._current_cbf_scan_id = scan_id

    @override
    async def last_scan_data_received(self) -> None:
        self._current_cbf_scan_id = None
        await self._force_pending_end_scan()

    async def _force_pending_end_scan(self):
        if not self._do_end_scan_task:
            return
        self._do_end_scan_task.cancel_delay()
        await self._do_end_scan_task
        self._do_end_scan_task = None
