"""Module to store the continous stats receivers."""

import asyncio
import json
import logging
import time

import numpy as np
from aiokafka import AIOKafkaProducer

logger = logging.getLogger(__name__)


class ContinuousStatsReceiver:
    """Class to handle receiving stats continously."""

    def __init__(self, receiver_instance, interval=1.0, max_stats=0):
        self.stats_receivers = []
        self.receiver_instance = receiver_instance
        self.task = None
        self.interval = interval
        self.scan_id = -1
        self.max_stats = max_stats
        self.stats_counter = 0

    def add_receiver(self, stats_receiver):
        """Adds a stats receiver to list of receivers."""
        self.stats_receivers.append(stats_receiver)

    async def start(self):
        """Start a coroutine that will receive stats and send on to listeners."""
        if len(self.stats_receivers) == 0:
            return
        for rec in self.stats_receivers:
            await rec.start()

        self.task = asyncio.create_task(self._collect())

    async def stop(self):
        """Request the receiving stats coroutine to stop."""
        if len(self.stats_receivers) == 0:
            return
        while self.max_stats > 0 and self.stats_counter < self.max_stats:
            await asyncio.sleep(self.interval / 2)
        self.task.cancel()
        for rec in self.stats_receivers:
            await rec.stop()

    async def _collect(self):
        while self.max_stats == 0 or self.stats_counter < self.max_stats:
            try:
                await self.collect_and_send_stats()
                await asyncio.sleep(self.interval)
            except asyncio.CancelledError:
                break
            self.stats_counter += 1

    async def collect_and_send_stats(self):
        """Collect and send stats to all receivers."""

        if self.scan_id == -1:
            sending_coros = [rec.send_no_stats(self.scan_id) for rec in self.stats_receivers]
        else:
            stats = self.receiver_instance.stats
            sending_coros = [rec.send_stats(self.scan_id, stats) for rec in self.stats_receivers]

        try:
            await asyncio.gather(*sending_coros)
        except Exception:  # pylint: disable=broad-except
            logger.exception("Unexpected error while sending stats")


class NpEncoder(json.JSONEncoder):
    """Helper class to encode Numpy data to JSON."""

    def default(self, o):
        """Helper for Numpy to JSON."""
        if isinstance(o, np.integer):
            return int(o)
        if isinstance(o, np.floating):
            return float(o)
        if isinstance(o, np.ndarray):
            return o.tolist()
        return json.JSONEncoder.default(self, o)


class KafkaStatsReceiver:
    """Receiver of stats that is meant to send to Kafka."""

    @classmethod
    def create(cls, config_string):
        """Create a receiver from the given config"""
        connection_details = config_string.split(":")
        topic = connection_details[-1]
        host = connection_details[0]

        if len(connection_details) == 3:
            port = connection_details[1]
        elif len(connection_details) == 2:
            port = "9092"
        else:
            raise ValueError("config_string must follow '<host>[:<port>]:<topic>'")
        return cls(host, port, topic)

    def __init__(self, host, port, topic):
        self.topic = topic
        self.connection_string = f"{host}:{port}"
        logger.info(
            "Created a Kafka Receiver, host=%s, topic=%s",
            self.connection_string,
            self.topic,
        )

    async def start(self):
        """Start the producer."""
        # pylint: disable-next=attribute-defined-outside-init
        self.aioproducer = AIOKafkaProducer(
            bootstrap_servers=self.connection_string,
            compression_type="gzip",
        )
        await self.aioproducer.start()

    async def send_stats(self, scan_id, stats):
        """Send the stats as is to a Kafka topic."""
        stats_json = {
            "type": "receive_stats",
            "time": time.time(),
            "scan_id": scan_id,
            "state": "stats",
            "total_megabytes": stats.total_bytes / 1024 / 1024,
            "num_heaps": stats.num_heaps,
            "num_incomplete": stats.num_incomplete,
            "duration": stats.duration,
            "streams": [
                {
                    "id": stream_num,
                    "heaps": stream_stats["heaps"],
                    "blocked": stream_stats["worker_blocked"],
                    "incomplete_heaps": stream_stats["incomplete_heaps_evicted"]
                    + stream_stats["incomplete_heaps_flushed"],
                }
                for stream_num, stream_stats in enumerate(stats.per_stream_stats)
            ],
        }
        await self.aioproducer.send(self.topic, json.dumps(stats_json, cls=NpEncoder).encode())

    async def send_no_stats(self, scan_id):
        """Send the stats as is to a Kafka topic."""
        stats_json = {
            "type": "receive_stats",
            "time": time.time(),
            "scan_id": scan_id,
            "state": "no_stats",
        }
        await self.aioproducer.send(self.topic, json.dumps(stats_json, cls=NpEncoder).encode())

    async def stop(self):
        """Flush and stop the producer."""
        await self.aioproducer.flush()
        await self.aioproducer.stop()
