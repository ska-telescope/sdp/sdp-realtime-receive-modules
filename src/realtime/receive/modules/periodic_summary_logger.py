import logging
import time


class PeriodicSummaryLogger:
    """
    A class that periodically logs a summary message. When it's time to log
    there might be no message, in which case no logging output is produced.

    Subclasses need to specify the logger, period and message to use for
    logging. The latter receives two named arguments, ``summary`` (its value
    and type is up to the user, see below) and ``elapsed_time``, a float
    indicating the number of seconds since the last report.

    The summary is specified by implementing the ``summary`` method, which can
    return any type of object. It's up to the user to format this summary into
    the message. The only requirement for ``summary`` is that no logging is
    produced if the result evalutes to ``False`` in a boolean context.
    """

    def __init__(self, logger, period, message, level=logging.INFO):
        self._logger: logging.Logger = logger
        self._period: float = period
        self._message: str = message
        self._level: int = level
        self._last_report_time: float = time.time()

    def reset(self):
        """Resets the summary after a full period has passed."""

    def summary(self):
        """
        The summary to log periodically, if any. Logged only if it evalutes to
        ``True`` at the moment of reporting.
        """
        raise NotImplementedError()

    def maybe_log(self, timestamp=None, force=False):
        """
        Log the summary, if any, if a full period has passed or if forced to do
        so.
        """
        timestamp = timestamp or time.time()
        elapsed_time = timestamp - self._last_report_time
        if elapsed_time < self._period and not force:
            return
        if summary := self.summary():
            self._logger.log(
                self._level,
                self._message,
                {"summary": summary, "elapsed_time": elapsed_time},
            )
        self._last_report_time = timestamp
        self.reset()
