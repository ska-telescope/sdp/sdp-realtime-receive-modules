"""Scan lifecycle handler interface definition"""

import abc


class ScanLifecycleHandler(abc.ABC):
    """
    An interface for objects that need to be notified about SDP scan transitions
    as they happen.
    """

    @abc.abstractmethod
    async def start_scan(self, scan_id: int) -> None:
        """Called when an SDP scan has started"""

    @abc.abstractmethod
    async def end_scan(self, scan_id: int) -> None:
        """Called when an SDP scan has ended"""
