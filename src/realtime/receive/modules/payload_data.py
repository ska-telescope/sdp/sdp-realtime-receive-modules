import typing

import numpy as np
from realtime.receive.core import Scan, icd


class PayloadData(typing.NamedTuple):
    """
    The subset of the ICD payload that we need to keep track of.

    ICD Payload objects in the spead2_receiver module are reused every time a
    new data heap arrives, meaning that the Items' values in their internal
    ``_ig`` ItemGroup are overwritten each time a data heap is unpacked into
    them. On the other hand, this reuse is necessary, becase the ItemGroup has
    the ItemDescriptors for those items, which are received at start-of-stream
    time, and without which we coulnd't unpack the data heaps.

    Data aggregation requires us to keep track of multiple Payloads' worth of
    data, but at the same time we need to reuse those Payload objects across
    time to receive further data heaps. Because of this we need a different way
    to keep a hold onto the data received from each data heap. This class
    achieves that purpose, being a smaller version of the icd.Payload that only
    contains the values of the Items received on each data heap. While
    conceptually these values are those from the data heap, they are not
    *exactly* those: in SKA Low the "timestamp" member shown here is made out
    of Item values sent in the data heap AND the start-of-stream heap.
    Similarly, some arrays are given an out dimension to match Low and Mid's
    dimensionality.
    """

    first_channel_id: int
    channel_count: int
    visibilities: np.ndarray
    timestamp: float
    correlated_data_fraction: np.ndarray
    sequence_number: int
    scan: Scan

    @staticmethod
    def from_payload(payload: icd.Payload, sequence_number: int, scan: Scan):
        """Create an instance of this class from an ICD Payload"""
        return PayloadData(
            first_channel_id=payload.channel_id,
            channel_count=payload.channel_count,
            visibilities=payload.visibilities,
            timestamp=payload.timestamp,
            correlated_data_fraction=payload.correlated_data_fraction,
            sequence_number=sequence_number,
            scan=scan,
        )
