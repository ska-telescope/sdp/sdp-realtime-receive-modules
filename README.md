SDP Realtime Receive Modules
============================

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-sdp-realtime-receive-modules/badge/?version=latest)](https://developer.skao.int/projects/ska-sdp-realtime-receive-modules/en/latest/?badge=latest)

This is the python package of realtime receive modules.

## Installation

Installation instructions can be found in the online [documentation](https://developer.skao.int/projects/ska-sdp-realtime-receive-modules/en/latest/).
