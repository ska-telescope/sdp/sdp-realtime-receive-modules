Change Log
###########

Development
-----------

6.0.0
-----

Added
^^^^^

 * Automated benchmarking to CI.
 * Benchmarks for payload aggregation.

Changed
^^^^^^^

 * Decreased amount of DeprecationWarnings printed by the receiver by default
   to remove flooding from logs coming from our Plasma usage.
 * Adjusted default value for aggregation payload backoff,
   scaling it according to the number of streams being received.
 * Detect how ``Scan`` and ``EndScan`` commands issued to the SDP Subarray
   purely based on the Execution Block state's ``scan_id`` value.
   Previously the subarray's ``command`` field was used,
   which both should be considered a private member of the subarray component,
   and also proved to be an unreliable source of truth
   given its volatile nature,
   and the fact that SDP Config watchers can miss updates
   when issued in rapid succession.
 * **BREAKING** Bumped dependency on SDP packages ``ska-sdp-config`` and
   ``ska-sdp-datamodels`` to ``^1.0.0``.

Optimised
^^^^^^^^^

 * Improved performance of payload aggregation for single-channel payloads
   (like in SKA Low). For small array sizes (6 stations, 1000 channels) this
   goes up by a factor of ~2x, while for bigger ones it is less pronounced (1.5x
   for 12 stations, 100 channels; 1.07x for 64 stations, 10 channels).
 * Improved performance of payload reception for the same use case above.
   Improvements are in the 30% range for AA0.5-like use cases.

5.2.4
-----

Added
^^^^^
 * Added support for SKA Repository Tooling (SKART)


5.2.3
-----

Changed
^^^^^^^

 * Changed the behaviour of spead2_receiver to loosen the test for
   the item descriptors. Now only raise an exception if there are
   missing item descriptors, not if there are extra ones.

5.2.2
-----

Changed
^^^^^^^

 * Changed the subarray endpoint to use the lmc-subarray and not the sdp-config subarray.


5.2.0
-----

Changed
^^^^^^^

* Updated dependency on ska-sdp-config to ``0.9.0``.

5.1.0
-----

Added
^^^^^

* A new ``aggregation.num_timestamps_per_aggregation`` option.
  This can be used with other aggregation options to control the aggregation of data.
  It is the number of integration intervals (timestamps) that will be aggregated together to form
  a single visibility. This logic has been introduced after the tolerance logic and
  should incorporate both paths. It will simply hold the accumulation until all the logical
  paths have been satisfied.

Updated
^^^^^^^

* Updated dependency on ska-sdp-config to ``0.8.0``.

5.0.2
-----

Updated
^^^^^^^

* Updated dependency on aiokafka to ``0.11.0``.

5.0.1
-----

Updated
^^^^^^^

* Updated lint dependency on black to ``24.4.2``.
* Updated lint dependency on flake8 to ``7.1.0``.
* Updated lint dependency on pylint to ``3.2.5``.



5.0.0
-----

.. note::

  The alpha releases leading up to this one were named ``4.3.0-alpha.*``,
  since the understanding was that these changes would be backwards-compatible.
  However, the addition of the new RPC calls invoked by the ``plasma_writer``
  meant that existing processors would not work anymore against the new receiver,
  and hence were backwards-incompatible.


Added
^^^^^

* Consumers have now two ``start_scan`` and ``end_scan`` asynchronous methods
  that are invoked when an Scan starts and ends in the system.
  In particular, the ``plasma_writer`` consumer methods
  (implemented during ``4.3.0-alpha.1``)
  will be invoked,
  triggering the correct RPC invocation on processors.
* For SDP-less deployments (e.g., when unit/integration tests)
  Scan transitions are emulated from the incoming stream of CBF data.
* If SDP goes through ``EndScan`` into the ``READY`` obsState,
  but CBF is still sending data,
  the receiver continues receiving data
  until CBF sends the end-of-stream heaps,
  or until a configurable a maximum amount of time (configurable) passes,
  after which the streams are forcibly shut.

Changed
^^^^^^^

* Payloads arriving before a Scan has started,
  or after a Scan has finished,
  are now dropped.

Deprecated
^^^^^^^^^^
* The contents of the ``realtime.receive.modules.scan_provider`` module
  has been broken down into individual modules
  under the new ``realtime.receive.modules.scan_providers`` package.
  The ``realtime.receive.modules.scan_provider`` module
  still provides the names, but issues a deprecation warning.

Fixed
^^^^^

* Failures happening during periodic aggregation
  will now be correctly handled.
  They previously did **not** log an error message,
  and they prevented further period aggregations
  from occurring.
* ``MSWriterConsumer`` will now handle spectral windows with a non-zero
  starting channel_id.
* Minor bug when trying to log reception statistics after streams are closed,
  but no start-of-stream heaps have arrived.

Changed
^^^^^^^

* Updated dependency on ska-sdp-realtime-receive-core to ``^6.2.0``.

Removed
^^^^^^^

* **BREAKING** Removed deprecated ``Consumer.stop`` method.


4.3.0-alpha.1
^^^^^^^^^^^^^

Added
^^^^^

* ``plasma_writer`` consumer now invokes
  the ``start_scan`` and ``end_scan``
  RPC calls supported by processors,
  informing them about scans starting and ending.


Changed
^^^^^^^

* Updated dependency on ska-sdp-realtime-receive-core to ``^6.1.0``.
* Updated dependency on ska-sdp-dal-schemas to ``^0.6.1``.

4.2.1
-----

Changed
^^^^^^^

* Updated dependency on ska-sdp-dal-schemas to ``~0.6.0``.

Fixed
^^^^^

* Allow aggregation of payloads out of time order.

4.2.0
-----

Changed
^^^^^^^

* Updated dependency on spead2 to ``^4.1.0``.
* Updated dependency on ska_sdp_cbf_emulator to ``^7.0.0``
  giving access to the new hardcoded emulation mode.
* Updated dependency on pyarrow to ``==11.0.0``
  to get binary wheels when installing under Python 3.11.

Fixed
^^^^^

* Fix premature aggregation of initial payloads.
  This would result in payloads being dropped earlier than
  they otherwise should have given the
  ``aggregation.integration_interval_tolerance`` value.

4.1.0
-----

Added
^^^^^

* New ``reception.reset_time_indexing_after_each_scan`` option
  to force resetting the time indexer after a scan's data
  has been received.
  This is useful for scenarios
  where different scans are sent with the same data,
  including the same timestamps,
  which would index into previously-seen sequence numbers.

Changed
^^^^^^^

* New ``PayloadAggregator.reset_time_indexing`` method
  to explicit request resetting of the internal time indexing,
  useful in some test scenarios.


4.0.0
-----

Note: this list includes all collapsed changes throughout the ``4.0.0-alpha.*``
releases, with small editions to present a coherent story.

Added
^^^^^

* Data received from the CBF is now aggregated
  before being handed over to the configured consumer.
  The ``aggregation`` settings group
  controls how aggregation works.
* New ``accumulating_consumer`` built-in consumer,
  which accumulates all data it receives in memory
  for later inspection,
  useful for testing.
* New ``tm.hardcoded_{num_receptors,auto_corr,lower_triangular}`` options
  to use fake, hard-coded station and baseline information
  during reception.
  Specifying the first is needed to trigger the behavior,
  while the other two affect how baselines are generated.
* A new ``scan_provider.hardcoded_spectral_window_channels`` option
  has been added to aid with testing of different channel configurations
  without requiring input Measurement Sets with such actual configurations.

Changed
^^^^^^^

* **BREAKING**
  ``Consumer.consume`` receives now a single ``Visibility`` object
  instead of a ``payload, payload_seq_no, scan`` triplet.
  ``payload_seq_numbers``, ``scan`` and ``channel_ids`` can be retrieved
  from the ``Visibility``'s ``attr["meta"]`` dictionary.
* **BREAKING**
  The ``ScanProviderConfig`` and ``TelescopeManagerConfig`` classes
  don't have an ``sdp_config_db`` member anymore,
  since now a single SDP Config DB client is created and shared internally.
* **BREAKING** Bump ``ska-sdp-realtime-receive-core`` to ``^6.0.0``.
* **BREAKING** Dropped support for dict-based configuration
  to be passed to ``receiver.receive``.
* **BREAKING** The file pointed to
  by the ``-c`` / ``--config`` command line option
  of ``vis-receive``
  is now a YAML file instead of an INI file.
* Data aggregation happens across time and frequency
  (which wasn't the case in all previous ``4.0.0-alpha.*`` releases).
  The creation of the resulting ``Visibility`` object
  happens continuously with a configurable period and tolerance,
  and it always includes all streams opened for reception.
* The ``reception.buffer_size`` default value
  now depends on the chosen protocol
  (it previously was hard-coded to be spead2's default value
  for UDP streams).
  It is also automatically adjusted
  to not go over the OS settings for maximum buffer sizes,
  thus avoiding warnings when the streams are created.
* Bump ``ska-sdp-config`` to ``^0.5.1``
  to use new backend used now throughout SDP.
* Bump aiokafka to 0.8.1 to enable faster CRC checks

Fixed
^^^^^

* Channel IDs are no longer assumed to start at 0 and to be
  contiguous. They are now determined by searching the scan's
  spectral window and the data received over spead.
  Note that if multiple beams are present and they have overlapping
  channel IDs, the wrong spectral window may be selected.
  This will be addressed in a subsequent release.
* Frequencies calculated for payloads is now correct
  (compared to previous ``4.0.0-alpha.*`` releases)
  in the face of strided Spectral Windows.
* The implementation of the ``wait_for_all_responses_timeout`` setting
  for the ``plasma_writer`` consumer was incorrect,
  leading to the receiver to terminate early,
  and producing unexpected failures on the processors downstream.

Removed
^^^^^^^

* The ``aggregation.propagate_consumer_errors`` option
  (introduced during the ``4.0.0-alpha.*`` releases).
  Now consumer errors are always gracefully handled,
  and don't result on the receiver finishing early.

4.0.0-alpha.2
-------------

Added
^^^^^

* Data received from the CBF is now aggregated
  before being handed over to the configured consumer.
  The ``aggregation`` settings group
  controls how aggregation works.
* New ``accumulating_consumer`` built-in consumer,
  which accumulates all data it receives in memory
  for later inspection,
  useful for testing.

Changed
^^^^^^^

* The ``reception.buffer_size`` default value
  now depends on the chosen protocol
  (it previously was hard-coded to be spead2's default value
  for UDP streams).
  It is also automatically adjusted
  to not go over the OS settings for maximum buffer sizes,
  thus avoiding warnings when the streams are created.
* The ``reception.propagate_consumer_errors`` setting
  has been moved to ``aggregation.propagate_consumer_errors``.

Fixed
^^^^^

* Channel IDs are no longer assumed to start at 0 and to be
  contiguous. They are now determined by searching the scan's
  spectral window and the data received over spead.
  Note that if multiple beams are present and they have overlapping
  channel IDs, the wrong spectral window may be selected.
  This will be addressed in a subsequent release.

4.0.0-alpha.1
-------------

Added
^^^^^

* New ``tm.hardcoded_{num_receptors,auto_corr,lower_triangular`` options
  to use fake, hard-coded station and baseline information
  during reception.
  Specifying the first is needed to trigger the behavior,
  while the other two affect how baselines are generated.
* New ``reception.propagate_consumer_errors`` option (defaults to ``False``)
  to fail quickly and exit from a receiver
  whose consumer raises an exception,
  useful for testing.

Changed
^^^^^^^

* **BREAKING**
  ``Consumer.consume`` receivs now a single ``Visibility`` object
  instead of a ``payload, payload_seq_no, scan`` triplet.
  For the time being a ``Visibility`` contains a single ``Payload``,
  and ``payload_seq_no``, ``scan`` and ``channel_id`` can be retrieved
  from the ``Visibility``'s ``attr["meta"]`` dictionary.
* **BREAKING**
  The ``ScanProviderConfig`` and ``TelescopeManagerConfig`` classes
  don't have an ``sdp_config_db`` member anymore,
  since now a single SDP Config DB client is created and shared internally.

3.10.2
------

Fixed
^^^^^

* The implementation of the ``wait_for_all_responses_timeout`` setting
  for the ``plasma_writer`` consumer was incorrect,
  leading to the receiver to terminate early,
  and producing unexpected failures on the processors downstream.

3.10.1
------

Added
^^^^^

* A new ``vis-receiver`` script is the main entry point
  for the receiver command-line tool.

Changed
^^^^^^^

* The ``emu-recv`` script is still present,
  but deprecated.
* ``UVWEngine`` is now instantiated using the baseline order
  specified in the Low CBF ICD.

3.10.0
------

Changed
^^^^^^^

* The baseline order assumed by the receiver,
  and currently hardcoded,
  has changed to match
  the one specified in the new Low CBF ICD.

Fixes
^^^^^

* Receiver now withstands errors raised by a consumer
  instead of immediately crashing.

3.9.2
-----

Fixed
^^^^^

* The implementation of the ``wait_for_all_responses_timeout`` setting
  for the ``plasma_writer`` consumer was incorrect,
  leading to the receiver to terminate early,
  and producing unexpected failures on the processors downstream.

3.9.1
-----

Changed
^^^^^^^

* Updated internal dependency on receive-core to ``^5.0.1`` to pick up
  fixes for missing measurement set entries


3.9.0
-----

Changed
^^^^^^^

* Updated internal dependency on receive-core to ``^5.0`` which incorporates
  a fix to the LowICD definition.


3.8.1
-----

Fixes
^^^^^

* ``plasma_writer`` consumer is now prevented
  from intermittently entering an invalid state
  for short periods of time.
* ``plasma_writer`` correctly keeps payloads in flight in check.
  The previous implementation allowed them, on occasion,
  to grow beyond the configured limit.

3.8.0
-----

New Features
^^^^^^^^^^^^

* Support reception of data sent
  using the new Low CSP SDP ICD.
* Support for "receiving" data from packet capture files.
  One file can be given via the new ``reception.pcap_file``,
  and it can contain multiple streams.
* Log reception of start-of-stream heaps,
  which were previously silently consumed.

Changed
^^^^^^^

* Updated internal dependency on receive-core to ``^4.0``.
  This involved a breaking change in the ``msutils`` module
  that our ``mswriter`` had to be adjusted for.

3.7.0
-----

New Features
^^^^^^^^^^^^

* Support to use the ``ska-telmodel`` package
  to retrieve antenna layout information.
  Two new options under the ``telescope_model`` group
  allow users to specify the key and a list of sources
  where the data will be retrieved from.
* A new ``reception.num_streams`` option governs
  the number of streams a receiver should open.
* Let the top-level ``receive`` function
  handle ``asyncio.CancelledError`` graciously,
  swallowing the exception, logging a message,
  and exiting cleanly.

Fixes
^^^^^

* Fixed remaining known issues with the ``plasma_writer`` consumer,
  where in certain situations it would enter an invalid state.

Deprecated
^^^^^^^^^^

* The ``reception.num_channels`` and ``reception.channels_per_stream`` options
  are now deprecated in favour of the ``reception.num_streams`` option.

3.6.0
-----

New Features
^^^^^^^^^^^^

* Added configuration classes
  for easier internal handling of configuration options.
* Added new configuration options
  to specify the different timeouts to use
  when reading results from output reference objects
  from the Plasma store
  in the ``plasma_writer`` consumer.

Improvements
^^^^^^^^^^^^

* ``Consumer`` objects are better managed now internally,
  with proper cleanup happening at reception shutdown
  instead of at the end of a stream reception,
  which better supports some use cases.
* Switched project build from setuputils to Poetry
* When reception finishes,
  the ``plasma_writer`` now explicitly disconnects
  from the Plasma Store,
  preventing ``OSError``\s we have seen in the past.
  It also waits for any pending responses (with a configurable timeout),
  preventing a known race condition
  for which we used to have an unsuitable workaround.

Deprecated
^^^^^^^^^^

* Passing a dict-like object to ``realtime.receive.modules.receiver.receive``.
  Users should use the new ``realtime.receive.modules.receiver.ReceiverConfig`` class instead.
* ``Consumer.stop`` deprecated in favour of ``Consumer.astop()`` coroutine.
* The following configuration items have been renamed
  (but are still available for backwards-compatibility):

  * ``reception.consumer`` -> ``consumer.name``.
  * ``reception.outputfilename`` -> ``consumer.output_filename``.
  * ``reception.max_payloads`` -> ``consumer.max_payloads_per_ms``.`
  * ``reception.command_template`` -> ``consumer.command_template``.`
  * ``reception.timestamp_output`` -> ``consumer.timestamp_output``.`
  * ``reception.plasma_path`` -> ``consumer.plasma_path``.
  * ``reception.payloads_in_flight`` -> ``consumer.payloads_in_flight``.

* The ``receiver.test_failure`` option,
  which should have been used only by tests,
  now has no effect and will be removed in future releases.

3.5.0
-----

* New high-level API entrypoint to run a fully-configured receiver code.
* Graciously handle out-of-order start-of-stream heap,
  with configurable queue size and loss period reporting
  for data heaps that are pending consumption.
* Deprecated several configuration options under the ``receiver`` group
  in favour of new specific configuration groups.
* Update Dockerfile to use `ubuntu:22.04` and do a proper multi-stage build

3.4.3
-----

* Updated Docker image to install pyarrow < 10
  so a plasma_store executable is still available
  and the receiver chart on SDP integration still works.
* Added option to disable
  the automatic download of astropy IERS data.

3.4.2
-----

* Bump the core dependency to 3.5.2 to pickup
  fixes.

3.4.1
-----

* Added the ability to choose the UVW calculator via configuration. There are
  only two choices at the moment katpoint and measures(casa).

* Updated to latest version of ska-sdp-dal-schema
  that correctly specifies the weights and flags dimensionality.

3.4.0
-----

* Updated core to 3.4.0 Changed the interface to Antenna to match

* Removed processors code from this repository
  since it's been moved into the newer
  `ska-sdp-realtime-receive-processors <https://gitlab.com/ska-telescope/sdp/ska-sdp-realtime-receive-processors>`__ project.
* Changed dimensionality and contents
  of flags and weights written into Plasma
  to match future expectations set
  by the Visibility datamodel class.

3.3.1
-----

* Patch to bump the core dependency verions to 3.3.2

3.3.0
-----

* Add new ``transport_protocol`` option
  to indicate the network transport used by spead2.
  Supported values are ``tcp`` and ``udp``, defaults to ``udp``.

3.2.1
-----

* Updated dependencies on receive-core and dal-schemas
  to get latest ADR-54 schema compliance.
* Adjusted codebase to latest dependencies.
* Added dependency on ska-sdp-dal as it has been removed from receive-core.

3.2.0
-----

* Simplified GitLab CI infrastructure to use existing templates.
* Removed test data from Docker image.
* Added code from receive-core that was specific to receive-modules
  into this repository, renaming a few modules and classes
  along the way.

3.1.1
-----

* Quick fix for emu-recv command line argument handling
  when trying to use the ``SdpConfigScanProvider``.

3.1.0
-----
* Added the UVWEngine to the consumer interface
  This permits the consumers to calculate their own
  uvw coordinates. The currrent engine uses the casacore
  measures functionality and generates J2000 uvw. There is
  a small difference between J2000 uvw and ICRS. But if the
  the delay direction is J2000 the difference will be very
  small. And probably a very small rotation of the field.
  THe relativistic corrections are probably a larger effect.

3.0.0
-----

* Added support for generating a measurement set per scan
* Updated to new ska-sdp-realtime-receive-core and ska-sdp-dal-schemas
* All consumers now receive a ``Scan`` object alongside each payload.
  This object contains scan-specific data,
  which was previously inferred from the TM.
* `plasma_writer` updated to new scan model
* Reimplemented `CommandExecutor` to new `CommandFileExecutor`
* Ported to use central ICD Item descriptions from receive-core
* Made plasma dependencies required, not optional.

2.1.0
-----

* Do a clean shutdown on ``SIGINT``
  (a handler for ``SIGTERM`` had been added in 2.0.4).
* Add ``readiness_file`` option
  to create an empty file when the receiver is ready setting itself up.
  This in turn can be used by k8s to avoid race conditions
  at pod startup time.
* Added ``buffer_size`` and ``max_packet_size`` receiver options
  to control spead2's corresponding settings.
  If anything, this can at least be used to silence runtime warnings
  due to the default buffer size being to big for vanilla kernel configurations.
* Add richer stats tracking infrastructure to the receiver.
  Stats are generally available at runtime for users to inspect,
  and in particular are logged at the end of the reception.
* Stopped using the ``transmission.channels_per_stream`` configuration option,
  use the ``reception.channels_per_stream`` option instead.
* Renamed current ``null_consumer`` to ``example_consumer``,
  as it is more like the latter.
  A new, empty ``null_consumer`` has been added
  for a no-op, CPU-free experience.

2.0.4
-----

* Fixed time index calculation in ``plasma_writer``,
  which was broken when used together with ``SchedTM``.
* Updated Docker image to include WSRT measures and install the package's plasma
  extra dependencies.

2.0.3
-----

Bumped the core dependency to 2.0.5

2.0.2
-----

Bumping the core dependency to 2.0.4

2.0.1
-----

There was a typo in the setup.py so the entry point was emu_recv instead of emu-recv - may even change that name entirely soon

2.0.0
-----

Major refactor into 3 repos. The core utils repo, the receive modules and the emulator. Hence the mahor version change. There is no change to functionality. This repository is the CORE repository it now contains the
interfaces and utility functions for both the receive modules and the emulator

No new dependencies have been introduced

Package name change realtime.receive.core


1.6.11
------

Reversed the logic in the handling of external models - so now scheduling block based models are prioritised


1.6.10
------

Adding the functionality to create a measurement set from the scheduling block. This replaces the need to have a data model. Essentially I have refactored the TM classes to inherit from a BaseTM abstract base class - there are now three:

The original FakeTM which takes a measurement set in construction
The PlasmaTM which uses the contents of the plasma store in construction
[new] SchedTM which uses a scheduling block instance.

The 'get_nearest_data" is overloaded in these cases to simply return an empty UVW vector. Which is added to the measurement set. This seems to work fine in all the test cases - but I have had to remove checking the 'value' of the UVW tables in the MSAsserter used in the unit tests - as there are no UVW.

* Added schedblock parameter that can be used instead of datamodel for telescope model constrution
* Removed the requirement to test the value of the UVW table in the tests

1.6.9
-----

* Added mswriter parameters 'timestamp_output', 'command_template' and 'max_payloads'
* Added receive loop with 'continuous_mode' parameter
* Added timestamped ms names

1.6.8
-----

* Bugfix for receiving data outside model timerange
* Hide ms opening log messages

1.6.7
-----

* Added ms-asserter entrypoint for testing pipeline outputs
* Added configurable ring buffer option to mswriter
* emu-send repeat option increments now timestamps
* emu-recv updated to allow receiving timestamps beyond the model range

1.6.6
-----

* Removed the copy from the Dockerfile. This will increase the size - but should fix
  the issues related to missing functionality in the image.

1.6.5
-----

* Small update to the default Docker image to include plasma

1.6.4
-----

* Added Dockerfiles for dockerised tests of emulator and receive workflows

1.6.3
-----

* changed the repo name again to place it in the sdp sub-group.
* Updated the dependencies on the DAL.

1.6.2
-----
* changed the repo name
* added the .release file


1.6.1
-----

* Version not released due to issues with publication on the CAR

1.6
---

* Added optional support for using plasmaStMan for the DATA column
  of the main table.
* Added option to execute commands
  for each newly created Measurement Set
  with ``plasma-mswriter``.
* Corrected spectral window values when writing measurement sets.
* Added a null_consumer that provides an example for developers. Functionally just drops the payloads
* Added new BDD tests and data for AA0.5LOW configurations
* Changed the functionality of ``reader.num_repeats`` - it now resends the number of timestamps set by
  ``reader.num_timestamps`` the stated number of times - each separated by ``transmission.time_interval``.

1.5
---

* Added new ``transmission.time_interval`` option
  to control the time to let pass between transmissions of data
  of successive timestamps.
  By default this is calculated
  from the ``TIME`` values of the input Measurement Set,
  but arbitrary, fixed intervals, as well as null intervals,
  can be specified.
* Removed our dependency on OSKAR to read and write Measurement Sets.
  The code now uses the python casacore bindings instead,
  leading to a simpler installation procedure,
  both for developers and users.
* Ported code to use new name/version of the SKA logging library.
* Fixed and simplified configuration requirements
  of few of the tools and modules,
  so running the system requires less values
  to be explicitly given in order to run.
* Added automatic publishing of python package to Nexus
  when tags are pushed.

1.4
---

* Ported the plasma caller and processor
  to use Arrow Tables to transport visibility metadata
  rather than binary tensors with pickled content.
  This makes it possible to invoke processors
  that do not use our data types,
  and that are potentially written in a different language.
* The schemas defining the Tables and procedures
  are imported from the ``ska-sdp-dal-schemas`` package.

1.3
---

* Fixed a problem with a test failing
  when the plasma extra was not installed.
* Pin sdp-dal-prototype dependency
  to avoid breaking changes upstream.
* Removed dependency on ``astropy``.
  This package adds ~50 [MB] of data to our installations,
  while in return we use a single bit of functionality
  that is easily implemented.
* Remove old, unnecessary dependency on ``sh`` package.

1.2
---

* Added ability to specify user-provided consumer classes,
  which will allow us to use third-party consumer code
  without us having to maintain it.
* Added ``unix_time`` property to ``Payload`` class,
  which is compatible with values from the ``time`` built-in module.
* Updated to to work with spead2 versions 2 and 3.
* Updated test infrastructure to work
  against latest version of pytest-bdd.

1.1
---

* Developed new reception consumer
  that writes incoming payload data
  into an Apache Plasma store
  using the `sdp-dal-prototype <https://gitlab.com/ska-telescope/sdp-dal-prototype>` code.
  In the sdp-dal-prototype parlance this is a "Caller".
* Developed a corresponding "Processor"
  that reads the data off the Plasma store
  and writes it into a Measurement Set.
* General bug fixes and improvements.


1.0.1
-----

* Minor adjustments
  to example data used by demonstrations.

1.0
---

* First public release.
* SPEAD-based transmission and reception of ICD payloads
  implemented.
* Receivers use a generic consumer architecture
  to handle incoming payloads offered.
* Specific consumer implemented
  that takes incoming payloads
  and write them into a Measurement Set.
* It is possible to transmit data for multiple channels
  through a single SPEAD stream.
* UDP multicast transmission works.
* Simple Helm charts demonstrating functionality
  are available as examples.

0.3
---

* Further fixes, still not fully operational

0.2
---

* First tag, still not fully operational
