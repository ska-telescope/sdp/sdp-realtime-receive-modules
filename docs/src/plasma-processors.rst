Plasma Processors
=================

When using the :ref:`consumers.plasma_writer` consumer,
incoming payloads are written to Plasma
as Arrow Tables and Tensors.
This writing is done via the :doc:`ska-sdp-dal <ska-sdp-dal:index>` package,
which implements an RPC-like API;
from its standpoint our ``plasma_writer`` is a *Caller*,
and the payload is written into plasma
representing a remote method invocation.
On the other side,
a *Processor* is expected to react to these writes.

This project originally contained
the code to support Processors.
However this has now been split
into the :doc:`ska-sdp-realtime-receive-processors:index` project.
Its documentation explains
how to write and run
a custom Processor,
or one of the built-in ones.

When using the ``ska-sdp-dal`` library,
both Callers and Processors
need to agree on the method signatures
(name, input types, etc).
To help with this,
the :doc:`ska-sdp-dal-schemas <ska-sdp-dal-schemas:index>` package
contains the shared definitions
that are needed for these methods to be used.
