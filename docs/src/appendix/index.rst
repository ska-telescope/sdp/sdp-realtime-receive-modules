Appendix
========

This is a collection of documents
that complement the core documentation.
Mind you that some of them might be out of data,
and refer to old versions of this package,
or even to other bits and pieces
of the data flow
(e.g., the sender emulator
that used to shared the same repository than this code).

.. toctree::

   running-vis-workflow
   sp-1857
