.. doctest-skip-all
.. _package-guide:


API documentation
=================

This section describes requirements and guidelines.

The receiver is very configurable - with different pluggable receivers (SPEAD2 being the initial implementation) and
multiple consumers (measurement set writers, plasma store writers etc). The payload is also selectable - but currently


High-level entrypoint
---------------------

.. autoclass:: realtime.receive.modules.receiver.SdpConfigDbConfig
   :members:

.. autoclass:: realtime.receive.modules.receiver.ScanProviderConfig
   :members:

.. autoclass:: realtime.receive.modules.receiver.TelescopeManagerConfig
   :members:

.. autoclass:: realtime.receive.modules.receiver.UVWEngineConfig
   :members:

.. autoclass:: realtime.receive.modules.receiver.ReceiverConfig
   :members:

.. autofunction:: realtime.receive.modules.receiver.receive


Receivers
---------

.. autoclass:: realtime.receive.modules.receivers.Config
    :members:

.. automodule:: realtime.receive.modules.receivers.spead2_receivers
    :members:

Aggregation
-----------

.. automodule:: realtime.receive.modules.aggregation
   :members:

Consumers
---------

.. autoclass:: realtime.receive.modules.consumers.Config
    :members:

.. automodule:: realtime.receive.modules.consumers.accumulating_consumer
    :members:

.. automodule:: realtime.receive.modules.consumers.mswriter
    :members:

.. automodule:: realtime.receive.modules.consumers.plasma_writer
    :members:


Telescope Managers
------------------

.. automodule:: realtime.receive.modules.tm.base_tm
    :members:

.. automodule:: realtime.receive.modules.tm.ms_tm
    :members:

.. automodule:: realtime.receive.modules.tm.sched_tm
    :members:

Scan Providers
--------------

.. automodule:: realtime.receive.modules.scan_provider
    :members:
