Installation
============

To install this package you will need the
following system requirements:

* Python 3.10 or greater
* Poetry 1.2.2 or greater

As described about you will need Poetry installed
to successfully setup a development environment.
If you install a published package via pip then you will
also need to ensure that you also point pip to the CAR server.::

  # Go into the top-level directory of this repository
  cd ska-sdp-realtime-receive-modules

  # For development, install all dependencies and the package in editable mode
  poetry install

  # To just use the package, install only main dependencies then the package
  poetry install --only main --no-root && pip install .

Note that this package can also be installed directly via pip
from the CAR itself::

 pip install --extra-index-url=https://artefact.skao.int/repository/pypi-all/simple \
   ska-sdp-realtime-receive-modules

Alternatively, if the latest development version is needed,
it can also be installed from the upstream git repository itself::

 pip install --extra-index-url=https://artefact.skao.int/repository/pypi-all/simple \
   'git+https://gitlab.com/ska-telescope/sdp/ska-sdp-realtime-receive-modules.git#egg=ska-sdp-realtime-receive-modules'
