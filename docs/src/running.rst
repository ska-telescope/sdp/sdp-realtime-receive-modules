Running
=======

From the command-line
---------------------

A :program:`vis-receiver` program should be available after installing the package.
Configuration options can be given through a configuration file or through the
command-line. :option:`vis-receiver -h` for details.


.. program:: vis-receiver

.. option:: -h

   Show help

.. option:: -c config-file

   The configuration file to load, default is empty

.. option:: -o OPTION

   Additional configuration options in the form of ``category.name=value``


A typical configuration file looks like this:

.. code-block:: yaml

   general:
     measurement_set: tests/data/gleam-model.ms

   reception:
     consumer: mswriter
     outputfilename: tests/data/recv-vis.ms


In practical terms it makes sense to start the receiver(s) before the transmitter
so they are waiting for data. But you do not have to - the protocols and consumers are
flexible enough to be started when the data-stream is already running.


Programmatically
----------------

A high-level
:func:`receive <realtime.receive.modules.receiver.receive>` coroutine
is available for users of this package
to setup, run a receiver,
and wait for its completion.
The coroutine hides all the complexity
of creating all the objects that are needed
to setup a fully-working receiver,
while giving full control to the user
on how to configure this setup.

The :program:`vis-receiver` is indeed a thin wrapper
around this high-level entry-point.


In SDP
------

For a more complex deployment of the receiver program
in the context of SDP
please see the documentation
of the :doc:`vis-receive script <ska-sdp-script:scripts/vis-receive>`,
part of the :doc:`ska-sdp-script:index`.
