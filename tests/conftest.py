import pytest
import ska_sdp_config
from common import EmulatedSDP


@pytest.fixture(name="sdp_config")
def sdp_config_fixture():
    """
    Return an SDP config DB client, and makes sure there's nothing under the
    `/eb` key.
    """
    sdp_config_client = ska_sdp_config.Config()
    sdp_config_client.backend.delete("/eb", recursive=True, must_exist=False)
    sdp_config_client.backend.delete("/lmc", recursive=True, must_exist=False)
    yield sdp_config_client
    sdp_config_client.close()


@pytest.fixture
def emulated_sdp(sdp_config):
    """A fixture that provides access to an emulated version of SDP"""
    with EmulatedSDP(sdp_config) as sdp:
        yield sdp
