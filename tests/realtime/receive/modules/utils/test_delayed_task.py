import asyncio

import pytest

from realtime.receive.modules.utils.delayed_task import DelayedTask


@pytest.mark.parametrize("delay", [0.001, 0.01, 0.1])
@pytest.mark.asyncio
async def test_delayed_task_invokes_callback(delay):
    lock = asyncio.Lock()
    await DelayedTask(delay, lock.acquire)
    assert lock.locked()


@pytest.mark.parametrize("delay", [10, 100, 1000])
@pytest.mark.asyncio
async def test_delayed_task_can_be_accelerated(delay):
    lock = asyncio.Lock()
    task = DelayedTask(delay, lock.acquire)
    await asyncio.sleep(0)
    assert not lock.locked()
    task.cancel_delay()
    await task
    assert lock.locked()


@pytest.mark.asyncio
async def test_cancellation():
    lock = asyncio.Lock()
    delayed_task = DelayedTask(10000, lock.acquire)

    async def _consume():
        await delayed_task

    wrapper_task = asyncio.create_task(_consume())
    wrapper_task.cancel()
    with pytest.raises(asyncio.CancelledError):
        await wrapper_task
    assert not lock.locked()
