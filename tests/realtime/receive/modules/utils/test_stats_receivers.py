import asyncio
import unittest
from unittest.mock import call, create_autospec

from realtime.receive.modules.utils.continuous_stats_receiver import (
    ContinuousStatsReceiver,
    KafkaStatsReceiver,
)

# pylint: disable=missing-class-docstring,missing-function-docstring


class FakeReceiver:
    @property
    def stats(self):
        return {"stats": "fakeness"}


async def run_test_async(csr):
    await csr.start()
    # This stop will not end immediatly.
    await csr.stop()


class TestStatsReceivers(unittest.TestCase):
    def test_receiver(self):
        mocked_receiver = create_autospec(KafkaStatsReceiver)

        csr = ContinuousStatsReceiver(FakeReceiver(), interval=0.1, max_stats=3)
        csr.add_receiver(mocked_receiver("host", "port", "port"))
        csr.scan_id = 5

        asyncio.run(run_test_async(csr))

        calls = [
            call("host", "port", "port"),
            call().start(),
            call().send_stats(5, {"stats": "fakeness"}),
            call().send_stats(5, {"stats": "fakeness"}),
            call().send_stats(5, {"stats": "fakeness"}),
            call().stop(),
        ]

        assert mocked_receiver.mock_calls == calls

    def test_receiver_no_scan_id(self):
        mocked_receiver = create_autospec(KafkaStatsReceiver)

        csr = ContinuousStatsReceiver(FakeReceiver(), interval=0.1, max_stats=3)
        csr.add_receiver(mocked_receiver("host", "port", "port"))

        asyncio.run(run_test_async(csr))

        calls = [
            call("host", "port", "port"),
            call().start(),
            call().send_no_stats(-1),
            call().send_no_stats(-1),
            call().send_no_stats(-1),
            call().stop(),
        ]

        assert mocked_receiver.mock_calls == calls

    def test_receiver_no_stats_calls(self):
        mocked_receiver = create_autospec(KafkaStatsReceiver)

        csr = ContinuousStatsReceiver(FakeReceiver(), interval=5)
        csr.add_receiver(mocked_receiver("host", "port", "port"))

        asyncio.run(run_test_async(csr))

        calls = [
            call("host", "port", "port"),
            call().start(),
            call().stop(),
        ]

        assert mocked_receiver.mock_calls == calls

    def test_kafka_create(self):
        ksr = KafkaStatsReceiver.create("host:topic")
        assert ksr.topic == "topic"
        assert ksr.connection_string == "host:9092"

        ksr = KafkaStatsReceiver.create("host:123:topic")
        assert ksr.topic == "topic"
        assert ksr.connection_string == "host:123"
