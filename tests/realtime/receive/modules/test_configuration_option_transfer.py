import copy
import warnings

import pytest

from realtime.receive.modules.receiver import _transfer_missing_options_from

SOURCE_CONFIG = {
    "group1": {
        "key1": "group1_value1",
        "key2": "group1_value2",
    },
    "group2": {
        "key1": "group2_value1",
        "key2": "group2_value2",
    },
}


def _transfer(*args, **kwargs):
    config = copy.deepcopy(SOURCE_CONFIG)
    _transfer_missing_options_from(config, "group1", *args, **kwargs)
    return config


def test_transfer_options():
    # A missing group appears and has new keys/values
    assert "group3" in _transfer("group3")
    with pytest.warns() as record:
        config = _transfer("group3", ("key1", "key1"), ("key2", "key2"))
    assert len(record) == 2
    assert config["group3"]["key1"] == SOURCE_CONFIG["group1"]["key1"]
    assert config["group3"]["key2"] == SOURCE_CONFIG["group1"]["key2"]

    # No re-writing of options that exist in target group
    for target_key in ("key1", "key2"):
        config = _transfer("group2", ("key1", "key2"))
        assert config["group2"][target_key] == SOURCE_CONFIG["group2"][target_key]

    # new keys appear in existing target groups
    with pytest.warns() as record:
        config = _transfer("group2", ("key1", "key3"))
    assert len(record) == 1
    assert config["group2"]["key3"] == SOURCE_CONFIG["group1"]["key1"]

    # no transfer of non-existing keys
    config = _transfer("group2", ("key-doesnt-exist", "key3"))
    assert "key3" not in config["group2"]

    # no warnings generated if requested
    with warnings.catch_warnings():
        warnings.simplefilter("error")
        config = _transfer("group2", ("key1", "key3"), no_warnings=True)
    assert config["group2"]["key3"] == SOURCE_CONFIG["group1"]["key1"]
