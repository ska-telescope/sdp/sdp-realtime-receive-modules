import pytest
from realtime.receive.core.katpoint_uvw_engine import KatpointUVWEngine
from realtime.receive.core.uvw_engine import UVWEngine

from realtime.receive.modules.receiver import UVWEngineConfig, create_uvw_engine
from realtime.receive.modules.tm.sched_tm import SKATelescopeManager

INPUT_FILE = "data/AA05LOW.ms"
SCHED_FILE = "data/assign_resources_1_0.json"
LAYOUT_FILE = "data/low-layout.json"


@pytest.mark.parametrize(
    "name, expectation",
    [
        ("katpoint", KatpointUVWEngine),
        (None, KatpointUVWEngine),
        ("cannot-heat-engine", ValueError),
        ("measures", UVWEngine),
        ("casa", UVWEngine),
    ],
)
def test_uvw_engine_creation(name, expectation):
    config_kwargs = {"engine": name} if name else {}
    config = UVWEngineConfig(**config_kwargs)
    tm = SKATelescopeManager(LAYOUT_FILE, SCHED_FILE)
    if issubclass(expectation, Exception):
        with pytest.raises(expectation):
            create_uvw_engine(config, tm)
    else:
        engine = create_uvw_engine(config, tm)
        assert isinstance(engine, expectation)
