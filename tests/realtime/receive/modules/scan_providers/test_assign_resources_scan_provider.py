from realtime.receive.modules.scan_providers import AssignResourcesScanProvider

ASSIGN_RESOURCES_COMMAND = "data/assign_resources_1_0.json"


def test_assign_resources_scan_provider():
    scan_provider = AssignResourcesScanProvider(ASSIGN_RESOURCES_COMMAND)
    scan0 = scan_provider.get(0)
    scan1 = scan_provider.get(1)
    assert scan0 != scan1


def test_scan_data_shape():
    scan_provider = AssignResourcesScanProvider(ASSIGN_RESOURCES_COMMAND)
    scan0 = scan_provider.get(0)
    assert (scan0.scan_type.num_channels, scan0.scan_type.num_pols) == (4, 4)
    scan1 = scan_provider.get(1)
    assert (scan1.scan_type.num_channels, scan1.scan_type.num_pols) == (4, 4)
