#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for the ms_tm module."""
import numpy as np
import pytest
from realtime.receive.core.msutils import MeasurementSet, Mode

from realtime.receive.modules.tm.ms_tm import MeasurementSetTM

RTOL = 1e-15
"""Relative Tolerance for floating point precision tests"""


def test_ms_tm():
    ms = MeasurementSet.open("data/sim-vis.ms", Mode.READONLY)
    tm = MeasurementSetTM(ms)
    assert "E3-1" == tm.get_antennas()[0].name
    np.testing.assert_allclose(tm.get_antennas()[0].x, -2561967.007031, rtol=RTOL)
    assert 4 == tm.num_stations
    assert 10 == tm.num_baselines


def test_faketm_fail():
    with pytest.raises(RuntimeError):
        MeasurementSetTM("IamNotHere")
