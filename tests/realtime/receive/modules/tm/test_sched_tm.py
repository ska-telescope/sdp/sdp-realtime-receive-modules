import os
import urllib.parse

import pytest
from realtime.receive.core.common import load_json_resource
from ska_sdp_config import Config, ExecutionBlock

from realtime.receive.modules.tm.sched_tm import SKATelescopeManager


def _assert_tm(tm):
    assert 4 == tm.num_stations
    assert 10 == tm.num_baselines
    assert tm.get_antennas()[0].name == "C1"


ASSIGN_RESOURCES_FILE = "data/assign_resources_1_0.json"
ANTENNA_LAYOUT_FILE = "data/low-layout.json"


def test_schedtm():
    _assert_tm(SKATelescopeManager(ANTENNA_LAYOUT_FILE, ASSIGN_RESOURCES_FILE))


def test_schedtm_fail():
    with pytest.raises(FileNotFoundError):
        SKATelescopeManager("IamNotHere")


EB_ID = "eb-test-00000000-0000"


@pytest.fixture(name="populated_sdp_config")
def _populated_sdp_config(sdp_config: Config):
    assign_resources_command = load_json_resource(ASSIGN_RESOURCES_FILE)
    eb_template = assign_resources_command["execution_block"]
    eb_template["key"] = EB_ID
    eb_template["resources"] = assign_resources_command["resources"]
    eb = ExecutionBlock.model_validate(eb_template)

    for txn in sdp_config.txn():
        txn.execution_block.create(eb)
    return sdp_config


def _to_in_memory_telmodel_uri(filename):
    with open(filename, "rb") as f:
        data = f.read()
    params = {os.path.basename(filename): data}
    return f"mem://?{urllib.parse.urlencode(params)}"


@pytest.mark.parametrize(
    "kwargs",
    (
        {"antenna_layout": "data/low-layout.json"},
        {"telmodel_key": "instrument/ska1_low/layout/low-layout.json"},
        {
            "telmodel_key": "low-layout.json",
            "telmodel_source_uris": [_to_in_memory_telmodel_uri("data/low-layout.json")],
        },
    ),
)
def test_from_sdp_config(sdp_config, populated_sdp_config, kwargs):
    assert populated_sdp_config  # otherwise it goes unused
    _assert_tm(SKATelescopeManager.from_sdp_config(EB_ID, sdp_config, **kwargs))
