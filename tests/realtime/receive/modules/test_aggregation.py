import asyncio
import itertools
import logging
import time
import typing

import numpy as np
import pytest
from pytest_benchmark.fixture import BenchmarkFixture
from realtime.receive.core import ChannelRange, baseline_utils, icd
from realtime.receive.core.uvw_engine import UVWEngine
from ska_sdp_datamodels.visibility import Visibility

from realtime.receive.modules import consumers
from realtime.receive.modules.aggregation import AggregationConfig, PayloadAggregator
from realtime.receive.modules.scan_providers import HardcodedScanProvider
from realtime.receive.modules.tm.hardcoded_tm import HardcodedTelescopeManager

SCAN_ID = 1

logger = logging.getLogger(__name__)


class AggregationCounts(typing.NamedTuple):
    """Tuple grouping different aggregation-related numbers."""

    added_payloads: int
    aggregated_payloads: int
    visibilities_generated: int
    visibilities_consumed: int

    @staticmethod
    def get(aggregator: PayloadAggregator):
        """Construct from the current state of a PayloadAggregator."""
        assert aggregator.visibilities_consumed == len(aggregator.consumer.visibilities)
        return AggregationCounts(
            aggregator.added_payloads,
            aggregator.aggregated_payloads,
            aggregator.visibilities_generated,
            aggregator.visibilities_consumed,
        )

    def __add__(self, other: "AggregationCounts") -> "AggregationCounts":
        return AggregationCounts(
            self.added_payloads + other.added_payloads,
            self.aggregated_payloads + other.aggregated_payloads,
            self.visibilities_generated + other.visibilities_generated,
            self.visibilities_consumed + other.visibilities_consumed,
        )


class AggregationSetup(typing.NamedTuple):
    """Tuple grouping different aggregation-related setup quantities"""

    num_stations: int
    channels: ChannelRange
    channels_per_payload: int

    @property
    def num_baselines(self) -> int:
        """Number of baselines"""
        return baseline_utils.baselines(self.num_stations, autocorr=True)

    @property
    def num_streams(self) -> int:
        """Number of streams"""
        assert self.channels.count % self.channels_per_payload == 0
        return self.channels.count // self.channels_per_payload


@pytest.fixture(scope="module", name="default_aggregation_setup")
def _default_aggregation_setup_fixture():
    return AggregationSetup(
        num_stations=6, channels=ChannelRange(0, 10, 1), channels_per_payload=1
    )


async def aggregator_factory(
    aggregation_setup: AggregationSetup,
    aggregation_config: AggregationConfig = AggregationConfig(),
) -> PayloadAggregator:
    """Build a PayloadAggregator useful for tests in this module."""
    tm = HardcodedTelescopeManager(aggregation_setup.num_stations)
    scan_provider = HardcodedScanProvider.from_spectral_window(aggregation_setup.channels)
    await scan_provider.first_scan_data_received(SCAN_ID)
    uvw_engine = UVWEngine(tm.get_antennas())
    # we always do manual flushes
    aggregation_config.time_period = 0
    aggregator = PayloadAggregator(
        aggregation_config,
        consumers.create(
            consumers.create_config(name="accumulating_consumer"),
            tm,
            uvw_engine,
        ),
        scan_provider,
        tm,
    )
    aggregator.inform_num_streams(aggregation_setup.num_streams)
    return aggregator


def _flat_list(grouped_payloads: list[list[icd.Payload]]) -> list[icd.Payload]:
    return list(itertools.chain.from_iterable(grouped_payloads))


def _dummy_payloads(
    aggregation_setup: AggregationSetup,
    *,
    num_time_steps: int = 1,
    time_start: float | None = None,
    time_step_period: float = 1,
) -> list[icd.Payload]:
    channels_per_payload = aggregation_setup.channels_per_payload
    num_baselines = aggregation_setup.num_baselines
    visibilities = np.zeros([channels_per_payload, num_baselines, 4], dtype="<c8")
    correlated_data_fraction = np.ones([channels_per_payload, num_baselines], dtype=np.uint8) * 255
    payloads = []
    time_start = time.time() if time_start is None else time_start
    channel_ids = aggregation_setup.channels.as_np_range()[::channels_per_payload]
    for time_step, channel_id in itertools.product(range(num_time_steps), channel_ids):
        payload = icd.Payload()
        payload.visibilities = visibilities
        payload.correlated_data_fraction = correlated_data_fraction
        payload.channel_id = channel_id
        payload.channel_count = channels_per_payload
        payload.timestamp = time_start + time_step * time_step_period
        payload.scan_id = SCAN_ID
        payloads.append(payload)

    num_streams = aggregation_setup.channels.count // channels_per_payload
    num_payloads = num_time_steps * num_streams
    assert num_payloads == len(payloads)
    return payloads


def _assert_visibility(
    visibility: Visibility,
    aggregation_setup: AggregationSetup,
    num_time_steps: int,
    flag_fraction: float,
):
    assert visibility.vis.shape == (
        num_time_steps,
        aggregation_setup.num_baselines,
        aggregation_setup.channels.count,
        4,
    )
    assert visibility.meta["channel_ids"][0] == aggregation_setup.channels.start_id
    flags = visibility.flags.data.ravel()
    assert np.allclose(len(np.nonzero(flags)[0]) / len(flags), flag_fraction)


def _assert_visibility_is_full(
    visibility: Visibility,
    aggregation_setup: AggregationSetup,
    num_time_steps: int,
):
    _assert_visibility(visibility, aggregation_setup, num_time_steps, flag_fraction=0)


async def _aggregate_fully_and_assert(
    payloads: list[icd.Payload],
    aggregator: PayloadAggregator,
    aggregation_setup: AggregationSetup,
    num_time_steps: int,
):
    num_payloads = len(payloads)

    # Do the aggregation
    counts_before = AggregationCounts.get(aggregator)
    await aggregator.add_payloads_and_flush(payloads)
    assert counts_before + AggregationCounts(
        num_payloads, num_payloads, 1, 1
    ) == AggregationCounts.get(aggregator)
    _assert_visibility_is_full(
        aggregator.consumer.visibilities[-1],
        aggregation_setup,
        num_time_steps,
    )


@pytest.mark.parametrize("channel_range_start", (0, 1, 100))
@pytest.mark.parametrize("channel_range_count", (4, 20, 100))
@pytest.mark.parametrize("channel_range_stride", (1, 2, 4))
@pytest.mark.parametrize("channels_per_payload", (1, 2, 4))
@pytest.mark.parametrize("num_time_steps", (1, 2, 10))
@pytest.mark.asyncio
async def test_single_full_aggregation(
    channel_range_start: int,
    channel_range_count: int,
    channel_range_stride: int,
    channels_per_payload: int,
    num_time_steps: int,
):
    """
    Test that full aggregation (i.e., all payloads are put into a single
    Visibility that is full) works across a range of SpectralWindow channel
    configurations, channels per payload, and number of time steps.
    """
    sw_channels = ChannelRange(channel_range_start, channel_range_count, channel_range_stride)
    aggregation_setup = AggregationSetup(
        num_stations=6, channels=sw_channels, channels_per_payload=channels_per_payload
    )

    aggregator = await aggregator_factory(aggregation_setup)
    payloads = _dummy_payloads(aggregation_setup, num_time_steps=num_time_steps)
    await _aggregate_fully_and_assert(payloads, aggregator, aggregation_setup, num_time_steps)


@pytest.mark.parametrize("num_time_steps", (1, 2, 10))
@pytest.mark.asyncio
async def test_consecutive_full_aggregations(
    num_time_steps, default_aggregation_setup: AggregationSetup
):
    """Test that multiple consecutive full aggregations work as intended"""
    aggregator = await aggregator_factory(default_aggregation_setup)
    for t in range(num_time_steps):
        payloads = _dummy_payloads(
            default_aggregation_setup,
            num_time_steps=1,
            time_start=t,
        )
        await _aggregate_fully_and_assert(
            payloads, aggregator, default_aggregation_setup, num_time_steps=1
        )


@pytest.mark.asyncio
async def test_dropping_late_payloads(default_aggregation_setup: AggregationSetup):
    t0 = time.time()
    payloads = [
        _dummy_payloads(default_aggregation_setup, time_start=t0 + i, num_time_steps=1)
        for i in range(5)
    ]

    aggregator = await aggregator_factory(
        default_aggregation_setup,
        AggregationConfig(timestamp_tolerance=2, num_timestamps_per_aggregation=1),
    )

    expected_counts = AggregationCounts.get(aggregator)

    # Add partial first time_step, no aggregation yet
    t0_added_payloads = payloads[0][:-2]
    expected_counts += AggregationCounts(len(t0_added_payloads), 0, 0, 0)
    await aggregator.add_payloads_and_flush(t0_added_payloads, ignore_tolerance=False)
    assert expected_counts == AggregationCounts.get(aggregator)

    # Add full second time_step, still no aggregation yet
    # (note: can't add to streams that haven't received data due to bug
    # shown in XFail test below)
    t1_added_payloads = payloads[1]
    expected_counts += AggregationCounts(len(t1_added_payloads), 0, 0, 0)
    await aggregator.add_payloads_and_flush(t1_added_payloads, ignore_tolerance=False)
    assert expected_counts == AggregationCounts.get(aggregator)

    # Add full third time_step, earliest data is older than tolerance,
    # Expecting aggregation
    t2_added_payloads = payloads[2]
    expected_counts += AggregationCounts(len(t2_added_payloads), 28, 3, 3)
    await aggregator.add_payloads_and_flush(t2_added_payloads, ignore_tolerance=False)
    assert expected_counts == AggregationCounts.get(aggregator)


@pytest.mark.asyncio
async def test_aggregation_by_timestep(default_aggregation_setup: AggregationSetup):
    """
    Test that the integration of a visibility is done by time step
    """
    aggregation_config = AggregationConfig(num_timestamps_per_aggregation=2)
    aggregator = await aggregator_factory(default_aggregation_setup, aggregation_config)
    payloads = _dummy_payloads(default_aggregation_setup, num_time_steps=10)

    # Add 1 time step , no aggregation yet
    t0_added_payloads = payloads[:-90]
    expected_counts = AggregationCounts(len(t0_added_payloads), 0, 0, 0)
    await aggregator.add_payloads_and_flush(t0_added_payloads, ignore_tolerance=False)
    assert expected_counts == AggregationCounts.get(aggregator)

    t0_added_payloads = payloads[10:20]
    # added another time step - still no aggregation
    await aggregator.flush(ignore_tolerance=False)
    expected_counts = AggregationCounts(20, 20, 1, 1)
    await aggregator.add_payloads_and_flush(t0_added_payloads, ignore_tolerance=False)

    assert expected_counts == AggregationCounts.get(aggregator)

    # add another time step - now we have 3 time steps
    t0_added_payloads = payloads[20:30]
    await aggregator.add_payloads_and_flush(t0_added_payloads, ignore_tolerance=False)

    expected_counts = AggregationCounts(30, 20, 1, 1)
    assert expected_counts == AggregationCounts.get(aggregator)

    t0_added_payloads = payloads[30:40]  # add another time step
    expected_counts = AggregationCounts(40, 40, 2, 2)
    await aggregator.add_payloads_and_flush(t0_added_payloads, ignore_tolerance=False)
    assert expected_counts == AggregationCounts.get(aggregator)


@pytest.mark.parametrize("late_payload_flushed_on_its_own", (True, False))
@pytest.mark.asyncio
async def test_late_arrival(
    late_payload_flushed_on_its_own, default_aggregation_setup: AggregationSetup
):
    """
    Tests that a payload arriving late doesn't make it into a Visibility, and
    doesn't interrupt further Visibilities being generated.
    """
    t0 = time.time()
    t1 = t0 + 1
    payloads_t0, payloads_t1 = [
        _dummy_payloads(default_aggregation_setup, num_time_steps=1, time_start=t)
        for t in (t0, t1)
    ]

    # Add all payloads for time t0 except the last one
    aggregator = await aggregator_factory(default_aggregation_setup)
    num_payloads = len(payloads_t0)
    await aggregator.add_payloads_and_flush(payloads_t0[:-1])
    assert AggregationCounts.get(aggregator) == AggregationCounts(
        num_payloads - 1, num_payloads - 1, 1, 1
    )
    assert len(aggregator.consumer.visibilities) == 1
    visibility = aggregator.consumer.visibilities[0]
    _assert_visibility(
        visibility, default_aggregation_setup, num_time_steps=1, flag_fraction=1 / num_payloads
    )

    # Add the last one
    aggregator.add_payload(payloads_t0[-1])
    if late_payload_flushed_on_its_own:
        await aggregator.flush()
        assert AggregationCounts.get(aggregator) == AggregationCounts(
            num_payloads, num_payloads - 1, 1, 1
        )
        await _aggregate_fully_and_assert(
            payloads_t1,
            aggregator,
            default_aggregation_setup,
            num_time_steps=1,
        )
    else:
        counts_before = AggregationCounts.get(aggregator)
        await aggregator.add_payloads_and_flush(payloads_t1)
        assert counts_before + AggregationCounts(
            num_payloads, num_payloads, 1, 1
        ) == AggregationCounts.get(aggregator)
        _assert_visibility_is_full(
            aggregator.consumer.visibilities[-1],
            default_aggregation_setup,
            num_time_steps=1,
        )


# we test even with a tolerance greater than the number of time steps
@pytest.mark.parametrize("num_time_steps", (10,))
@pytest.mark.parametrize("tolerance", list(range(12)))
@pytest.mark.asyncio
async def test_tolerance(
    num_time_steps: int, tolerance: int, default_aggregation_setup: AggregationSetup
):
    """Check that the tolerance setting is respected."""

    t0 = time.time()
    all_payloads = [
        _dummy_payloads(
            default_aggregation_setup,
            num_time_steps=1,
            time_start=t0 + delta_t,
        )
        for delta_t in range(num_time_steps)
    ]

    # Add all payloads for the first time of the time steps, and half of the
    # payloads for the later half of the time steps
    assert num_time_steps % 2 == 0
    half_time_steps = num_time_steps // 2
    num_payloads_per_time_step = len(all_payloads[0])
    half_of_payloads_in_time_step = num_payloads_per_time_step // 2
    payloads_to_add = _flat_list(
        all_payloads[0:half_time_steps]
        + [payloads[:half_of_payloads_in_time_step] for payloads in all_payloads[half_time_steps:]]
    )
    assert (
        len(payloads_to_add)
        == half_time_steps * num_payloads_per_time_step
        + half_time_steps * half_of_payloads_in_time_step
    )
    aggregation_config = AggregationConfig(timestamp_tolerance=tolerance)
    aggregator = await aggregator_factory(default_aggregation_setup, aggregation_config)

    # Depending on the tolerance setting, payloads from incomplete time steps
    # will be put into the resulting Visibility or not. Time steps with data
    # in all streams will always be included though
    expected_vis_incomplete_time_steps = max(half_time_steps - tolerance, 0)
    expected_vis_time_steps = half_time_steps + expected_vis_incomplete_time_steps
    expected_aggregated_payloads = (
        half_time_steps * num_payloads_per_time_step
        + expected_vis_incomplete_time_steps * half_of_payloads_in_time_step
    )
    expected_flag_fraction = (
        expected_vis_incomplete_time_steps * half_of_payloads_in_time_step
    ) / (expected_vis_time_steps * num_payloads_per_time_step)

    counts_before = AggregationCounts.get(aggregator)
    await aggregator.add_payloads_and_flush(payloads_to_add, ignore_tolerance=False)
    counts_after = AggregationCounts.get(aggregator)
    assert (
        counts_before + AggregationCounts(len(payloads_to_add), expected_aggregated_payloads, 1, 1)
        == counts_after
    )
    visibility = aggregator.consumer.visibilities[-1]
    _assert_visibility(
        visibility, default_aggregation_setup, expected_vis_time_steps, expected_flag_fraction
    )


@pytest.mark.asyncio
async def test_concurrent_flushes(default_aggregation_setup: AggregationSetup):
    """Make sure concurrent flushes produce Visibilities sorted by time."""

    num_time_steps = 100
    t0 = time.time()
    all_payloads = [
        _dummy_payloads(
            default_aggregation_setup,
            num_time_steps=1,
            time_start=t0 + delta_t,
        )
        for delta_t in range(num_time_steps)
    ]
    num_payloads = num_time_steps * default_aggregation_setup.num_streams

    aggregation_config = AggregationConfig(payloads_per_backoff=1)
    aggregator = await aggregator_factory(default_aggregation_setup, aggregation_config)
    concurrent_flushes = [
        aggregator.add_payloads_and_flush(_flat_list(payloads_slice))
        for payloads_slice in (all_payloads[:-2], all_payloads[-2:])
    ]
    await asyncio.gather(*concurrent_flushes)
    assert AggregationCounts(num_payloads, num_payloads, 2, 2) == AggregationCounts.get(aggregator)
    for visibility, expected_time_steps in zip(
        aggregator.consumer.visibilities, (num_time_steps - 2, 2)
    ):
        _assert_visibility_is_full(
            visibility, default_aggregation_setup, num_time_steps=expected_time_steps
        )
    seq_numbers = [vis.meta["payload_seq_numbers"][0] for vis in aggregator.consumer.visibilities]
    assert seq_numbers == [0, num_time_steps - 2]


@pytest.mark.parametrize("reset_time_indexing", (True, False))
@pytest.mark.asyncio
async def test_time_indexing_resetting(
    reset_time_indexing: bool, default_aggregation_setup: AggregationSetup
):
    """
    Check that time indexing resetting works as expected, allowing the same
    payloads to be aggregated even after a flush.
    """
    payloads = _dummy_payloads(default_aggregation_setup)
    num_payloads = len(payloads)

    # First flush of data
    aggregator = await aggregator_factory(default_aggregation_setup)
    await aggregator.add_payloads_and_flush(payloads)
    expected_counts = AggregationCounts(num_payloads, num_payloads, 1, 1)
    assert expected_counts == AggregationCounts.get(aggregator)

    # Second flush, should work if we requested time indexing, fail othrewise
    if reset_time_indexing:
        aggregator.reset_time_indexing()
        expected_counts += AggregationCounts(num_payloads, num_payloads, 1, 1)
    else:
        expected_counts += AggregationCounts(num_payloads, 0, 0, 0)
    await aggregator.add_payloads_and_flush(payloads)
    assert expected_counts == AggregationCounts.get(aggregator)

    # Under no circumstance can we reset time indexing if payloads have been
    # added since the last flush
    aggregator.reset_time_indexing()
    aggregator.add_payload(payloads[0])
    with pytest.raises(RuntimeError):
        aggregator.reset_time_indexing()


@pytest.mark.asyncio
async def test_initial_time_stamps_only_aggregated_once_tolerance_exceeded(
    default_aggregation_setup: AggregationSetup,
):
    t0 = time.time()
    payloads = [
        _dummy_payloads(default_aggregation_setup, time_start=t0 + i, num_time_steps=1)
        for i in range(3)
    ]

    aggregator = await aggregator_factory(
        default_aggregation_setup,
        AggregationConfig(timestamp_tolerance=2),
    )

    expected_counts = AggregationCounts.get(aggregator)

    # Add first time_step, no aggregation yet
    t0_added_payloads = payloads[0][:-2]
    expected_counts += AggregationCounts(len(t0_added_payloads), 0, 0, 0)
    await aggregator.add_payloads_and_flush(t0_added_payloads, ignore_tolerance=False)
    assert expected_counts == AggregationCounts.get(aggregator)

    # Add second time_step, still no aggregation yet
    t1_added_payloads = payloads[1][2:]
    expected_counts += AggregationCounts(len(t1_added_payloads), 0, 0, 0)
    await aggregator.add_payloads_and_flush(t1_added_payloads, ignore_tolerance=False)
    assert expected_counts == AggregationCounts.get(aggregator)

    # Add third time_step, earliest data is older than tolerance,
    # so it should be aggregated
    t2_added_payloads = payloads[2][::2]
    expected_counts += AggregationCounts(len(t2_added_payloads), len(t0_added_payloads), 1, 1)
    await aggregator.add_payloads_and_flush(t2_added_payloads, ignore_tolerance=False)
    assert expected_counts == AggregationCounts.get(aggregator)
    assert 2 / 10 == aggregator.visibilities_flagged_fraction


@pytest.mark.asyncio
async def test_initial_time_stamp_aggregated_if_all_streams_have_received_data(
    default_aggregation_setup: AggregationSetup,
):
    t0 = time.time()
    payloads = [
        _dummy_payloads(default_aggregation_setup, time_start=t0 + i, num_time_steps=1)
        for i in range(2)
    ]

    aggregator = await aggregator_factory(
        default_aggregation_setup,
        AggregationConfig(timestamp_tolerance=2),
    )

    expected_counts = AggregationCounts.get(aggregator)

    # Add partial first time_step, no aggregation yet
    t0_added_payloads = payloads[0][:-2]
    expected_counts += AggregationCounts(len(t0_added_payloads), 0, 0, 0)
    await aggregator.add_payloads_and_flush(t0_added_payloads, ignore_tolerance=False)
    assert expected_counts == AggregationCounts.get(aggregator)

    # Add partial second time_step, still no aggregation yet
    # (note: can't add to streams that haven't received data due to bug
    # shown in XFail test below)
    t1_added_payloads = payloads[1][:-2]
    expected_counts += AggregationCounts(len(t1_added_payloads), 0, 0, 0)
    await aggregator.add_payloads_and_flush(t1_added_payloads, ignore_tolerance=False)
    assert expected_counts == AggregationCounts.get(aggregator)

    # Add missing bit of first time_step, we now have all payloads for the
    # time_step so it should be aggregated
    t0_missing_payloads = payloads[0][-2:]
    expected_counts += AggregationCounts(len(t0_missing_payloads), len(payloads[0]), 1, 1)
    await aggregator.add_payloads_and_flush(t0_missing_payloads, ignore_tolerance=False)
    assert expected_counts == AggregationCounts.get(aggregator)
    assert 0 == aggregator.visibilities_flagged_fraction


@pytest.mark.asyncio
async def test_adding_payloads_out_of_order(
    default_aggregation_setup: AggregationSetup,
):
    t0 = time.time()
    payloads = [
        _dummy_payloads(default_aggregation_setup, time_start=t0 + i, num_time_steps=1)
        for i in range(2)
    ]

    aggregator = await aggregator_factory(
        default_aggregation_setup,
        AggregationConfig(timestamp_tolerance=2),
    )

    expected_counts = AggregationCounts.get(aggregator)

    # Add partial first time_step, no aggregation yet
    first_added_payloads = payloads[0][:-2]
    expected_counts += AggregationCounts(len(first_added_payloads), 0, 0, 0)
    await aggregator.add_payloads_and_flush(first_added_payloads, ignore_tolerance=False)
    assert expected_counts == AggregationCounts.get(aggregator)

    # Add partial second time_step, data added for streams that didn't
    # receive data for the first time step, still no aggregation yet
    second_added_payloads = payloads[1][2:]
    expected_counts += AggregationCounts(len(second_added_payloads), 0, 0, 0)
    await aggregator.add_payloads_and_flush(second_added_payloads, ignore_tolerance=False)
    assert expected_counts == AggregationCounts.get(aggregator)

    # Add missing bit of first time_step, aggregation is expected as we have
    # a full time step but the aggregator expects payloads to be added
    # in order so it raises an AssertionError
    first_missing_payloads = payloads[0][-2:]
    expected_counts += AggregationCounts(len(first_missing_payloads), len(payloads[0]), 1, 1)
    await aggregator.add_payloads_and_flush(first_missing_payloads, ignore_tolerance=False)
    assert expected_counts == AggregationCounts.get(aggregator)
    assert 0 == aggregator.visibilities_flagged_fraction


@pytest.mark.benchmark(group="aggregation_payload_addition")
@pytest.mark.parametrize("num_stations, num_channels", ([6, 1000], [18, 100], [64, 10]))
def test_benchmark_add_payloads(
    benchmark: BenchmarkFixture,
    num_stations: int,
    num_channels: int,
):
    """
    Benchmarks the addition of payloads into the aggregator. It does *not* include
    flushing them.
    """
    aggregation_setup = AggregationSetup(
        num_stations, channels=ChannelRange(0, num_channels), channels_per_payload=1
    )
    payloads = _dummy_payloads(aggregation_setup, num_time_steps=1000)

    async def async_add_payloads():
        aggregator = await aggregator_factory(aggregation_setup)
        t0 = time.monotonic()
        for payload in payloads:
            aggregator.add_payload(payload)
        duration = time.monotonic() - t0
        logger.info(
            "Added %d payloads in %.3f [s] (%.3f [us] per payload)",
            len(payloads),
            duration,
            duration * 1e6 / len(payloads),
        )

    def add_payloads():
        asyncio.run(async_add_payloads())

    # We have enough payloads that a single run is representative enough
    assert len(payloads) >= 10000
    benchmark.pedantic(add_payloads)


@pytest.mark.benchmark(group="aggregation_flush")
@pytest.mark.parametrize("payloads_per_backoff", [10, 100, 10000])
@pytest.mark.parametrize("num_stations, num_channels", ([6, 1000], [18, 100], [64, 10]))
def test_benchmark_flush(
    benchmark: BenchmarkFixture,
    payloads_per_backoff: int,
    num_stations: int,
    num_channels: int,
):
    """
    Benchmarks the aggregator.flush operation. It does *not* include the addition
    of payloads into the aggregator.
    """

    aggregation_setup = AggregationSetup(
        num_stations, channels=ChannelRange(0, num_channels), channels_per_payload=1
    )
    payloads = _dummy_payloads(aggregation_setup, num_time_steps=1000)

    # Here we need to create the aggregator and add payloads to it outside the
    # benchmark itself, which cannot be an async function. We also want all asyncio objects
    # to belong to the same loop, so we need to manually create it, use it, and close it.
    loop = asyncio.new_event_loop()
    aggregator = loop.run_until_complete(
        aggregator_factory(
            aggregation_setup, AggregationConfig(payloads_per_backoff=payloads_per_backoff)
        )
    )
    for payload in payloads:
        aggregator.add_payload(payload)

    async def async_flush():
        t0 = time.monotonic()
        await aggregator.flush()
        duration = time.monotonic() - t0
        logger.info(
            "Flushed %d payloads in %.3f [s] (%.3f [us] per payload)",
            len(payloads),
            duration,
            duration * 1e6 / len(payloads),
        )

    def flush():
        loop.run_until_complete(async_flush())

    try:
        # We have enough payloads that a single run is representative enough
        assert len(payloads) >= 10000
        benchmark.pedantic(flush)
    finally:
        loop.close()
