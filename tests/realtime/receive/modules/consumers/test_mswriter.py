#!/usr/bin/env python
import tempfile

from realtime.receive.modules import consumers
from realtime.receive.modules.tm import MeasurementSetTM


def test_mswriter_setup():
    output_filename = tempfile.mktemp(".ms", "output")
    config = consumers.create_config(name="mswriter", output_filename=output_filename)
    INPUT_FILE = "data/sim-vis.ms"

    myconsumer = consumers.create(config, MeasurementSetTM(INPUT_FILE))
    assert myconsumer
