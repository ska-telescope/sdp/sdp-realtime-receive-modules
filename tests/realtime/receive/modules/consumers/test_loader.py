import pytest

from realtime.receive.modules.consumers import (
    Config,
    accumulating_consumer,
    example_consumer,
    mswriter,
    null_consumer,
    plasma_writer,
)
from realtime.receive.modules.consumers._loader import _get_consumer_class
from realtime.receive.modules.consumers.consumer import Consumer


class _MockConsumer(Consumer):
    pass


@pytest.mark.parametrize(
    ["name", "expected_class"],
    [
        ("accumulating_consumer", accumulating_consumer.AccumulatingConsumer),
        ("mswriter", mswriter.MSWriterConsumer),
        ("null_consumer", null_consumer.NullConsumer),
        ("plasma_writer", plasma_writer.PlasmaWriterConsumer),
        ("example_consumer", example_consumer.ExampleConsumer),
        (f"{__name__}.{_MockConsumer.__name__}", _MockConsumer),
    ],
)
def test_get_consumer_class(name: str, expected_class: type):
    klass = _get_consumer_class(Config(name))
    assert klass == expected_class


class _MockInvalidConsumer:
    pass


def test_invalid_consumer_warns(caplog: pytest.LogCaptureFixture):
    klass = _get_consumer_class(Config(f"{__name__}.{_MockInvalidConsumer.__name__}"))
    assert klass == _MockInvalidConsumer
    assert "not a subclass" in caplog.text
