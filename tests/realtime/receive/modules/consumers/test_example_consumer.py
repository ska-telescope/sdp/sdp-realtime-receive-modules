import asyncio
import unittest

import numpy as np
from realtime.receive.core import icd, msutils, time_utils

from realtime.receive.modules.consumers import example_consumer
from realtime.receive.modules.payload_data import PayloadData
from realtime.receive.modules.scan_providers import MSScanProvider
from realtime.receive.modules.tm.ms_tm import MeasurementSetTM
from realtime.receive.modules.utils import sdp_visibility


class TestNullConsumer(unittest.TestCase):
    INPUT_FILE = "data/AA05LOW.ms"

    def test_consume(self):
        async def _test_consume():
            await asyncio.gather(self.run_consume())

        loop = asyncio.new_event_loop()
        loop.run_until_complete(_test_consume())
        loop.close()

    async def run_consume(self):
        """
        simple consumer of the the input file for testing
        """
        c = example_consumer.ExampleConsumer()
        # fake payload from INPUT file
        payload = icd.Payload()
        scan_provider = MSScanProvider(self.INPUT_FILE)
        tm = MeasurementSetTM(self.INPUT_FILE)

        async for vis, timestamp in msutils.vis_reader(self.INPUT_FILE, num_timestamps=1):
            n_baselines, n_freq = vis.shape[0:2]
            payload.timestamp = time_utils.mjd_to_unix(timestamp)
            payload.channel_count = n_freq
            payload.visibilities = icd.ms_to_icd(vis)
            payload.correlated_data_fraction = np.ones((n_freq, n_baselines), dtype=np.uint8)
            visibility = sdp_visibility.create_visibility(
                [PayloadData.from_payload(payload, 0, scan_provider.get(0))],
                tm,
            )
            await c.consume(visibility)


if __name__ == "__main__":
    unittest.main()
