"""Simple SDP emulation via SDP Config DB"""

import enum

from realtime.receive.core.common import load_json_resource
from ska_sdp_config import Config, ExecutionBlock


class SubarrayCommand(enum.Enum):
    """SDP Subarray Tango commands as seen in the Subarray's Config DB entry"""

    ASSIGN_RESOURCES = "AssignResources"
    """
    EMPTY -> IDLE. Not actually seen during receiver execution, but potentially
    assumed to be the last command executed on the Subarray when the receiver
    starts.
    """

    CONFIGURE = "Configure"
    """IDLE -> READY"""

    SCAN = "Scan"
    """READY -> SCANNING"""

    END_SCAN = "EndScan"
    """SCANNING -> READY"""

    END = "End"
    """READY -> IDLE, only briefly observed before the receiver is shut down"""

    NONE = None
    """No command has been issued to the Subarray"""


class EmulatedSDP:
    """
    An emulation of SDP and its state as seen in the SDP Config DB.
    """

    EB_ID = "eb-test-00000000-0000"
    SUBARRAY_ID = "01"

    def __init__(self, sdp_config: Config):
        self.sdp_config = sdp_config
        assign_resources_command = load_json_resource("data/assign_resources_1_0.json")
        eb_template = assign_resources_command["execution_block"]
        eb_template["key"] = EmulatedSDP.EB_ID
        eb_template["subarray_id"] = EmulatedSDP.SUBARRAY_ID
        eb = ExecutionBlock.model_validate(eb_template)

        subarray = {"command": SubarrayCommand.ASSIGN_RESOURCES.value}

        # Put the EB in the database in its pristine state

        for txn in self.sdp_config.txn():
            txn.execution_block.create(eb)
            txn.execution_block.state(eb).create({})
            self.subarray_component(txn).create_or_update(subarray)

        self.eblock: ExecutionBlock = eb
        self.subarray: dict = subarray
        self.scan_type_id: int = eb.scan_types[0]["scan_type_id"]

    def __enter__(self):
        return self

    def __exit__(self, _exc, _typ, _tb):
        for txn in self.sdp_config.txn():
            self.subarray_component(txn).delete()

    def subarray_component(self, txn):
        """Return the SDP Config DB entity for the emulated subarray"""
        return txn.component(f"lmc-subarray-{EmulatedSDP.SUBARRAY_ID}")

    def update_db_state(self, eb_values: dict, subarray_values: dict):
        """Update the given entities' fields in the SDP Config DB."""
        for txn in self.sdp_config.txn():
            # update eb state
            eb_state = txn.execution_block.state(self.eblock.key).get()
            eb_state = eb_state if eb_state is not None else {}
            eb_state.update(eb_values)
            txn.execution_block.state(self.eblock.key).update(eb_state)
            # subarray entity contains state
            self.subarray.update(subarray_values)
            self.subarray_component(txn).update(self.subarray)

    def _run_subarray_command(self, command: SubarrayCommand, scan_id: int | None = None):
        # SDP LMC issues two updates for each command, signaling when the command
        # starts and ends executing
        for current_command in (command.value, None):
            self.update_db_state(
                {"scan_type": self.scan_type_id, "scan_id": scan_id},
                {"command": current_command},
            )

    def run_scan_command(self, scan_id: int):
        """Emulate a Scan command on the SDP config DB"""
        self._run_subarray_command(SubarrayCommand.SCAN, scan_id)

    def run_end_scan_command(self):
        """Emulate an EndScan command on the SDP config DB"""
        self._run_subarray_command(SubarrayCommand.END_SCAN)
